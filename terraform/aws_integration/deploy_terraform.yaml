AWSTemplateFormatVersion: 2010-09-09
Description: >-
  Terraform AWS Integration.
  Create DynamoDB encrypted (managed by AWS KMS), Bucket S3 encrypted (SSE-S3) and Role/Group/User for Terraform.
Parameters:
  #> - Tags Variables
  TagApplication:
    Description: >-
      This tag is use to define the name of the application (lowercase).
    Type: String
    Default: "terraform"
  TagClient:
    Description: >-
      This tag is use to define the name of the client (lowercase).
    Type: String
  TagEnv:
    Description: >-
      This tag is use to define the name of the environnement (lowercase).
    Type: String
  TagIac:
    Description: >-
      This tag is use to define the type of IaC (lowercase).
    Type: String
    Default: "cfn"
  TagOwner:
    Description: >-
      This tag is use to define the owner of this project (lowercase).
    Type: String
  TagRepoGit:
    Description: >-
      This tag is use to define the git repo (lowercase).
    Type: String
  #> - Configuration
  TerraformBackendRoleExternalId:
    Description: >-
      A unique identifier that is required when you assume the "Terraform Backend" role.
      (32 characters and only letters and digits)
    Type: String
    NoEcho: true
    MinLength: "32"
    MaxLength: "32"
    AllowedPattern: ^[a-zA-Z0-9]*$
  TerraformDeployRoleExternalId:
    Description: >-
      A unique identifier that is required when you assume the "Terraform Backend" role.
      (32 characters and only letters and digits)
    Type: String
    NoEcho: true
    MinLength: "32"
    MaxLength: "32"
    AllowedPattern: ^[a-zA-Z0-9]*$
Resources:
  #> - DynamoDB
  TerraformBackendDynamoDB:
    Type: AWS::DynamoDB::Table
    Properties:
      TableName: !Sub "ddb-${TagApplication}-${TagEnv}-lock-state"
      AttributeDefinitions:
        - AttributeName: "LockID"
          AttributeType: "S"
      KeySchema:
        - AttributeName: "LockID"
          KeyType: HASH
      TableClass: STANDARD
      BillingMode: PAY_PER_REQUEST
      SSESpecification:
        SSEEnabled: true
      DeletionProtectionEnabled: true
      Tags:
        - Key: Name
          Value: !Sub "ddb-${TagApplication}-${TagEnv}-lock-state"
        - Key: Application
          Value: !Ref TagApplication
        - Key: Client
          Value: !Ref TagClient
        - Key: Env
          Value: !Ref TagEnv
        - Key: IaC
          Value: !Ref TagIac
        - Key: Owner
          Value: !Ref TagOwner
        - Key: RepoGit
          Value: !Ref TagRepoGit
  #> - Bucket S3
  TerraformBackendS3:
    Type: "AWS::S3::Bucket"
    DeletionPolicy: Retain
    Properties:
      BucketName: !Sub "s3-${TagApplication}-${TagEnv}-state"
      OwnershipControls:
        Rules:
          - ObjectOwnership: BucketOwnerEnforced # ACLs disabled
      PublicAccessBlockConfiguration:
        BlockPublicAcls: false
        BlockPublicPolicy: false
        IgnorePublicAcls: false
        RestrictPublicBuckets: false
      VersioningConfiguration:
        Status: Enabled
      BucketEncryption:
        ServerSideEncryptionConfiguration:
          - BucketKeyEnabled: true # Encryption type = SSE-S3
      Tags:
        - Key: Name
          Value: !Sub "s3-${TagApplication}-${TagEnv}-state"
        - Key: Application
          Value: !Ref TagApplication
        - Key: Client
          Value: !Ref TagClient
        - Key: Env
          Value: !Ref TagEnv
        - Key: IaC
          Value: !Ref TagIac
        - Key: Owner
          Value: !Ref TagOwner
        - Key: RepoGit
          Value: !Ref TagRepoGit
  #> - Terraform Backend Role
  TerraformBackendRole:
    Type: "AWS::IAM::Role"
    Properties:
      RoleName: !Sub "rol-${TagApplication}-${TagEnv}-backend"
      Description: "Role use by Terraform for the Backend."
      AssumeRolePolicyDocument:
        {
          "Version": "2012-10-17",
          "Statement":
            [
              {
                "Effect": "Allow",
                "Action": "sts:AssumeRole",
                "Principal": { "AWS": { "Fn::Sub": "${AWS::AccountId}" } },
                "Condition": {
                  "StringEquals": {
                    "sts:ExternalId": { "Ref" : "TerraformBackendRoleExternalId" }
                  }
                }
              }
            ]
        }
      Tags:
        - Key: Name
          Value: !Sub "rol-${TagApplication}-${TagEnv}-backend"
        - Key: Application
          Value: !Ref TagApplication
        - Key: Client
          Value: !Ref TagClient
        - Key: Env
          Value: !Ref TagEnv
        - Key: IaC
          Value: !Ref TagIac
        - Key: Owner
          Value: !Ref TagOwner
        - Key: RepoGit
          Value: !Ref TagRepoGit
  TerraformBackendS3Policy:
    Type: "AWS::IAM::Policy"
    Properties:
      Roles:
        - !Ref TerraformBackendRole
      PolicyName: !Sub "pol-${TagApplication}-${TagEnv}-s3-state"
      PolicyDocument:
        {
          "Version": "2012-10-17",
          "Statement": [
            {
              "Sid": "TerraformBackendS3",
              "Effect": "Allow",
              "Action": [
                "s3:PutObject",
                "s3:GetObject",
                "s3:ListBucket",
                "s3:DeleteObject"
              ],
              "Resource": [
                { "Fn::Join" : [ "", [ { "Fn::GetAtt": [ "TerraformBackendS3", "Arn" ] }, "/*" ] ] },
                { "Fn::GetAtt": [ "TerraformBackendS3", "Arn" ] }
              ]
            }
          ]
        }
  TerraformBackendDynamoDBPolicy:
    Type: "AWS::IAM::Policy"
    Properties:
      Roles:
        - !Ref TerraformBackendRole
      PolicyName: !Sub "pol-${TagApplication}-${TagEnv}-ddb-lock-state"
      PolicyDocument:
        {
          "Version": "2012-10-17",
          "Statement": [
            {
              "Sid": "VisualEditor0",
              "Effect": "Allow",
              "Action": [
                "dynamodb:PutItem",
                "dynamodb:DescribeTable",
                "dynamodb:DeleteItem",
                "dynamodb:GetItem"
              ],
              "Resource": [
                { "Fn::GetAtt": [ "TerraformBackendDynamoDB", "Arn" ] }
              ]
            }
          ]
        }
  #> - Terraform Deploy Role
  TerraformDeployRole:
    Type: "AWS::IAM::Role"
    Properties:
      RoleName: !Sub "rol-${TagApplication}-${TagEnv}-deploy"
      Description: "Role use by Terraform for deploy some ressources."
      AssumeRolePolicyDocument:
        {
          "Version": "2012-10-17",
          "Statement":
            [
              {
                "Effect": "Allow",
                "Action": "sts:AssumeRole",
                "Principal": { "AWS": { "Fn::Sub": "${AWS::AccountId}" } },
                "Condition": {
                  "StringEquals": {
                    "sts:ExternalId": { "Ref" : "TerraformDeployRoleExternalId" }
                  }
                }
              }
            ]
        }
      ManagedPolicyArns:
        - "arn:aws:iam::aws:policy/AdministratorAccess"
      Tags:
        - Key: Name
          Value: !Sub "rol-${TagApplication}-${TagEnv}-deploy"
        - Key: Application
          Value: !Ref TagApplication
        - Key: Client
          Value: !Ref TagClient
        - Key: Env
          Value: !Ref TagEnv
        - Key: IaC
          Value: !Ref TagIac
        - Key: Owner
          Value: !Ref TagOwner
        - Key: RepoGit
          Value: !Ref TagRepoGit
  #> - Terraform User
  TerraformUser:
    Type: AWS::IAM::User
    Properties:
      UserName: !Sub "usr-${TagApplication}-${TagEnv}-deploy-cicd"
      Policies:
        - PolicyName: !Sub "pol-${TagApplication}-${TagEnv}-sts-rol"
          PolicyDocument:
            {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Sid": "STSRoleForTerraform",
                  "Effect": "Allow",
                  "Action": "sts:AssumeRole",
                  "Resource": [
                    { "Fn::GetAtt": [ "TerraformBackendRole", "Arn" ] },
                    { "Fn::GetAtt": [ "TerraformDeployRole", "Arn" ] }
                  ]
                }
              ]
            }
      Tags:
        - Key: Name
          Value: !Sub "usr-${TagApplication}-${TagEnv}-deploy-cicd"
        - Key: Application
          Value: !Ref TagApplication
        - Key: Client
          Value: !Ref TagClient
        - Key: Env
          Value: !Ref TagEnv
        - Key: IaC
          Value: !Ref TagIac
        - Key: Owner
          Value: !Ref TagOwner
        - Key: RepoGit
          Value: !Ref TagRepoGit
  #> - Terraform User AccessKey
  TerraformUserAccessKey:
    Type: AWS::IAM::AccessKey
    Properties: 
      UserName: !Ref TerraformUser
      Status: "Active"
  #> - Terraform User SecretsManager
  # AWS CloudFormation doesn't support creating a SecureString parameter type.
  TerraformUserSecretsManager:
    Type: AWS::SecretsManager::Secret
    Properties:
      Name: !Sub "${TagEnv}/${TagApplication}/usr_terraform"
      Description: "The Access Key ID and Secret Access Key of the user Terraform."
      SecretString: !Sub '{"AWS_ACCESS_KEY_ID":"${TerraformUserAccessKey}","AWS_SECRET_ACCESS_KEY":"${TerraformUserAccessKey.SecretAccessKey}"}'
      Tags:
        - Key: Name
          Value: !Sub "smgr-${TagApplication}-${TagEnv}-usr_terraform"
        - Key: Application
          Value: !Ref TagApplication
        - Key: Client
          Value: !Ref TagClient
        - Key: Env
          Value: !Ref TagEnv
        - Key: IaC
          Value: !Ref TagIac
        - Key: Owner
          Value: !Ref TagOwner
        - Key: RepoGit
          Value: !Ref TagRepoGit