# Terraform AWS Integration

## TO DO

- [ ] Créer une Bucket Policy pour limiter l'acces au role.
- [ ] Créer une rotation automatique pour les AWS_ACCESS_KEY_ID/AWS_SECRET_ACCESS_KEY.

## ARCHI

<p align="center">
		<a href="https://gitlab.com/tw_nk_e/opscheatsheet/-/raw/main/LICENSE">
			<img alt="Deploy Terraform" src="deploy_terraform.png">
		</a>
	</p>