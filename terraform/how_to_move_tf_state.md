# How to move Terraform State

In my case, I would like to migrate Terrafom state in an old S3 to new S3 bucket.

1. Create a local copy of the Terraform State:
```bash
terraform state pull > terraform.tfstate.bkp20230922
```
2. Rename the file:
```bash
cp terraform.tfstate.bkp20230922 terraform.tfstate
```
3. Remove `.terraform` directory:
```bash
rm -rf .terraform
```
4. Configure the new remote Backend.
5. Move your state to the new remote Backend:
```bash
terraform init -migrate-state -backend-config="backend/prd.tfbackend"

Initializing the backend...
Do you want to copy existing state to the new backend?
  Pre-existing state was found while migrating the previous "local" backend to the
  newly configured "s3" backend. No existing state was found in the newly
  configured "s3" backend. Do you want to copy this state to the new "s3"
  backend? Enter "yes" to copy and "no" to start with an empty state.

  Enter a value: yes


Successfully configured the backend "s3"! Terraform will automatically
use this backend unless the backend configuration changes.

Initializing provider plugins...
- Reusing previous version of hashicorp/aws from the dependency lock file
- Reusing previous version of hashicorp/random from the dependency lock file
- Reusing previous version of hashicorp/null from the dependency lock file
- Installing hashicorp/aws v4.67.0...
- Installed hashicorp/aws v4.67.0 (signed by HashiCorp)
- Installing hashicorp/random v3.5.1...
- Installed hashicorp/random v3.5.1 (signed by HashiCorp)
- Installing hashicorp/null v3.2.1...
- Installed hashicorp/null v3.2.1 (signed by HashiCorp)

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```
6. Delete unnecessary files:
```bash
rm terraform.tfstate terraform.tfstate.backup
```

---
Source :
- [How can I move Terraform state in an old S3 bucket to a new S3 bucket?](https://stackoverflow.com/questions/69735414/how-can-i-move-terraform-state-in-an-old-s3-bucket-to-a-new-s3-bucket)