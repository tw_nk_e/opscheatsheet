# Certificat

## Installer un Certificat

### Amazon Linux 2023

> RHEL / CentOS / Fedora

*Instead of manually specifying the CA certificate with each command, we can add our internal CA certificates to the CA trust provided by the `ca-certificates` package. This package provides a directory structure in `/etc/pki/` to manage the certificates and a command `update-ca-trust` to manage the "consolidated and dynamic configuration of CA certificates and associated trust.*

Le dossier `/etc/pki/ca-trust/extracted` regroupe l'ensemble des CRTs de la machine :
```bash
cat /etc/pki/ca-trust/extracted/openssl/ca-bundle.trust.crt | grep "GlobalSign"
# GlobalSign Code Signing Root E45
# GlobalSign Code Signing Root R45
# GlobalSign ECC Root CA - R4
# GlobalSign ECC Root CA - R5
# GlobalSign Root CA
# GlobalSign Root CA - R3
# GlobalSign Root CA - R6
# GlobalSign Root E46
# GlobalSign Root R46
# GlobalSign Secure Mail Root E45
# GlobalSign Secure Mail Root R45
```

Pour ajouter un nouveau CRT au format PEM ou DER, pousser le dans le dossier `/etc/pki/ca-trust/source/anchors/` :
```bash
sudo su -
cd /etc/pki/ca-trust/source/anchors/
wget https://truststore.pki.rds.amazonaws.com/eu-west-1/eu-west-1-bundle.pem
chmod 644 /etc/pki/ca-trust/source/anchors/eu-west-1-bundle.pem
chown root:root /etc/pki/ca-trust/source/anchors/eu-west-1-bundle.pem
```

Mettez à jour le magasin de CRT :
```bash
sudo update-ca-trust
```

Vérifier que le CRT à bien été importé :
```bash
cat /etc/pki/ca-trust/extracted/openssl/ca-bundle.trust.crt | grep "RDS"
# Amazon RDS eu-west-1 Root CA RSA2048 G1
# Amazon RDS eu-west-1 Root CA RSA4096 G1
# Amazon RDS eu-west-1 Root CA ECC384 G1
```

---

SRC:

- https://www.redhat.com/sysadmin/ca-certificates-cli