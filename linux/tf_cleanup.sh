#!/bin/bash -e

# INIT COLOR
BLACK=$'\e[0;30m'
RED=$'\e[0;31m'
GREEN=$'\e[0;32m'
YELLOW=$'\e[0;33m'
BLUE=$'\e[0;34m'
PURPLE=$'\e[0;35m'
CYAN=$'\e[0;36m'
WHITE=$'\e[0;37m'
BOLD_BLACK=$'\e[1;30m'
BOLD_RED=$'\e[1;31m'
BOLD_GREEN=$'\e[1;32m'
BOLD_YELLOW=$'\e[1;33m'
BOLD_BLUE=$'\e[1;34m'
BOLD_PURPLE=$'\e[1;35m'
BOLD_CYAN=$'\e[1;36m'
BOLD_WHITE=$'\e[1;37m'
COLOR_RESET=$'\e[0m'

# FLAG
notice_flag() {
  local NOTICE_FLAG="${BOLD_CYAN}[✦]${COLOR_RESET}"
  local text="$1"
  echo -e "${NOTICE_FLAG} ${text}"
}
QUESTION_FLAG="${BOLD_BLUE}[?]${COLOR_RESET}"
WARNING_FLAG="${BOLD_YELLOW}[!]${COLOR_RESET}"
error_flag() {
  local ERROR_FLAG="${BOLD_RED}[✘]${COLOR_RESET}"
  local text="$1"
  echo -e "${ERROR_FLAG} ${text}"
}
check_flag() {
  local CHECK_FLAG="${BOLD_GREEN}[✔︎]${COLOR_RESET}"
  local text="$1"
  echo -e "${CHECK_FLAG} ${text}"
}
ANSWER_FLAG="${BOLD_PURPLE}[➤]${COLOR_RESET}"
################################################################################
if [ -z "$1" ]; then
  error_flag "Please specify the working directory as the first argument (./tf_cleanup.sh /path/...)"
  exit 1
fi

working_directory="$1"
if [ "$1" = "." ]; then
  working_directory=$(pwd)
fi

check_flag "Working directory is ${working_directory}"

terraform_directory=$(find "${working_directory}" -type d -name ".terraform")
terraform_lock_hcl_file=$(find "${working_directory}" -type f -name ".terraform.lock.hcl")
terragrunt_cache_directory=$(find "${working_directory}" -type d -name ".terragrunt-cache")
################################################################################
notice_flag "Find .terraform.lock.hcl"
total_size=0
file_count=0

for file in $terraform_lock_hcl_file; do
  if [ -f "${file}" ]; then
    file_size=$(du "${file}" | awk '{print $1}')
    total_size=$((total_size + file_size))
    file_count=$((file_count + 1))
    rm -f ${file}
  fi
done

total_size_kib=$((total_size * 512 / 1024))
total_size_mib=$((total_size * 512 / 1048576))
total_size_gib=$((total_size * 512 / 1073741824))

check_flag "Number of files deleted: ${file_count}"
check_flag "Total size of deleted files: ${total_size} blocks (${total_size_kib} kiB / ${total_size_mib} MiB / ${total_size_gib} GiB)"
################################################################################
notice_flag "Find .terraform/"
total_size=0
directory_count=0

for directory in $terraform_directory; do
  if [ -d "${directory}" ]; then
    dir_size=$(du -s "${directory}" | awk '{print $1}')
    total_size=$((total_size + dir_size))
    directory_count=$((directory_count + 1))
    rm -rf ${directory}
  fi
done

total_size_kib=$((total_size * 512 / 1024))
total_size_mib=$((total_size * 512 / 1048576))
total_size_gib=$((total_size * 512 / 1073741824))

check_flag "Number of folders deleted: ${directory_count}"
check_flag "Total size of deleted folders: ${total_size} blocks (${total_size_kib} kiB / ${total_size_mib} MiB / ${total_size_gib} GiB)"
################################################################################
notice_flag "Find .terragrunt-cache/"
total_size=0
directory_count=0

for directory in $terragrunt_cache_directory; do
  if [ -d "${directory}" ]; then
    dir_size=$(du -s "${directory}" | awk '{print $1}')
    total_size=$((total_size + dir_size))
    directory_count=$((directory_count + 1))
    rm -rf ${directory}

  fi
done

total_size_kib=$((total_size * 512 / 1024))
total_size_mib=$((total_size * 512 / 1048576))
total_size_gib=$((total_size * 512 / 1073741824))

check_flag "Number of folders deleted: ${directory_count}"
check_flag "Total size of deleted folders: ${total_size} blocks (${total_size_kib} kiB / ${total_size_mib} MiB / ${total_size_gib} GiB)"
