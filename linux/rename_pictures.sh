#!/bin/bash -e

if ! command -v exiftool >/dev/null 2>&1; then
    echo "La commande exiftool n'est pas présente sur ce système."
    exit 1
fi

if ! command -v jq >/dev/null 2>&1; then
    echo "La commande jq n'est pas présente sur ce système."
    exit 1
fi

# Obtenir la liste des fichiers JPG et NEF
file_list=$(ls *.mp4 *.HEIC)

# Compter le nombre total de fichiers
file_count=$(echo "${file_list}" | wc -l)

# Initialiser une variable pour le suivi du nombre de fichiers traités
processed_count=0

for picture in ${file_list}; do
    # create_date=$(exiftool -json "${picture}" | jq -r ".[].CreateDate" | sed -E 's/://g; s/ /-/') # 20220529-163030
    catch_date=$(stat -f "%SB %N" "${picture}" | awk '{print $1 " " $2 " " $3 " " $4}' | tr '[:upper:]' '[:lower:]')
    create_date=$(LANG=en_US.UTF-8 date -jf "%b %d %H:%M:%S %Y" "${catch_date}" "+%Y%m%d-%H%M%S")
    extension="${picture##*.}"
    new_filename="${create_date}.${extension}"
    
    # Tenter de renommer le fichier
    if ! mv "${picture}" "${new_filename}"; then
        echo "Erreur lors de la copie, mais en continuant la boucle..."
    fi

    # Incrémenter le compteur de fichiers traités
    ((processed_count++))

    # Calculer le pourcentage complété
    percentage=$((processed_count * 100 / file_count))
    echo -ne "Progress: ${percentage}%\r"  # Afficher sans nouvelle ligne
done
echo  # Saut de ligne pour le message final