<dl>
	<h1 align="center">
		<a href="https://gitlab.com/tw_nk_e/opscheatsheet">
			<img alt="OpsCheatSheet" src="https://gitlab.com/tw_nk_e/opscheatsheet/-/raw/main/src/vador_animate.gif" width="200">
		</a>
		<br>OpsCheatSheet<br>
	</h1>
</dl>

<dl>
	<p align="center">
		<a href="https://gitlab.com/tw_nk_e/opscheatsheet/-/raw/main/LICENSE">
			<img alt="License" src="https://img.shields.io/badge/license-TBW%20-yellow?style=flat-square">
		</a>
	</p>
</dl>

<div align="center">
	<sub>Built with ❤︎ by tw_nk_e 🇫🇷.</sub>
</div>
<br>

<dl>
	<p align="center">
		OpsCheatSheet is a repo collecting a set of "reminders" that I use daily in my professional life. I hope you'll find some tips that might interest you 🤓. I hope to have helped you, enjoy 🦄 ! 
	</p>
</dl>

<!-- ## Table of content

- AWS
	* [AWS CloudWatch Agent - Windows Server](https://gitlab.com/tw_nk_e/opscheatsheet/-/blob/main/aws/cloudatch_agent_win_srv.md)
- AWS CloudFormation
	* [Virtal Private Cloud](https://gitlab.com/tw_nk_e/opscheatsheet/-/tree/main/aws/cloudformation#virtual-private-cloud-vpc)
- Linux
	* [LVM (Logical Volume Manager)](https://gitlab.com/tw_nk_e/opscheatsheet/-/blob/main/linux/lvm.md)
- SSH
	* [SSH Key](https://gitlab.com/tw_nk_e/opscheatsheet/-/blob/main/ssh/ssh_key.md) -->

## License
This project is [THE BEER-WARE LICENSE](https://gitlab.com/tw_nk_e/opscheatsheet/-/blob/main/LICENSE) licensed.
