# Site-to-Site VPN Crypto
## Configuration

Phase encryption algorithms :
|   Algorithms  | Description                      | Status | Reco. by |
|:-------------:|----------------------------------|:------:|:--------:|
|     AES128    | 128 bit AES-CBC                  |  good  |   NIST   |
|     AES256    | 256 bit AES-CBC                  |  good  |   CNSA   |
| AES128-GCM-16 | 128 bit AES-GCM with 128 bit ICV |  good  |     -    |
| AES256-GCM-16 | 256 bit AES-GCM with 128 bit ICV |  good  |   CNSA   |

Phase integrity algorithms : 
| Algorithms |         Description         | Status |  Reco. by |
|:----------:|:---------------------------:|:------:|:---------:|
|    SHA1    |      SHA1 HMAC (96 bit)     |  weak! |     -     |
|  SHA2-256  | SHA2_256_128 HMAC (128 bit) |  good  | NIST/CNSA |
|  SHA2-384  | SHA2_384_192 HMAC (192 bit) |  good  |    CNSA   |
|  SHA2-512  | SHA2_512_256 HMAC (256 bit) |  good  |     -     |

Phase DH group numbers :
| Group (IANA) |    Keyword   |  Modulus  | Prime Size | Status | Reco. by |
|:------------:|:------------:|:---------:|:----------:|:------:|:--------:|
|       2      |   modp1024   | 1024 bits |      _     |  weak! |     -    |
|       5      |   modp1536   | 1536 bits |      -     |  weak! |     -    |
|      14      |   modp2048   | 2048 bits |      -     |  good  |     -    |
|      15      |   modp3072   | 3072 bits |      -     |  good  |   NIST   |
|      16      |   modp4096   | 4096 bits |      -     |  good  |     -    |
|      17      |   modp6144   | 6144 bits |      -     |  good  |     -    |
|      18      |   modp8192   | 8192 bits |      -     |  good  |     -    |
|      19      |    ecp256    |     -     |  256 bits  |  good  |     -    |
|      20      |    ecp384    |     -     |  384 bits  |  good  |   CNSA   |
|      21      |    ecp521    |     -     |  521 bits  |  good  |     -    |
|      22      | modp1024s160 | 1024 bits |      -     |  weak! |     -    |
|      23      | modp2048s224 | 2048 bits |      -     |  weak! |     -    |
|      24      | modp2048s256 | 2048 bits |      -     |  weak! |     -    |

## Weak Cryptographic Algorithms

Encryption: 
```txt
des,3des,cast,blowfish
```
Integrity Protection / Pseudo Random Functions:
```txt
md5,sha1
```
Diffie-Hellman Groups: 
```txt
modp512,modp768,modp1024,modp1024s160,modp1536,modp2048s224,modp2048s256,ecp192
```

---
Source: 
- [Cipher Selection](https://docs.strongswan.org/docs/5.9/howtos/securityRecommendations.html#_cipher_selection)
- [Diffie Hellman Groups](https://docs.strongswan.org/docs/5.9/config/IKEv2CipherSuites.html#_diffie_hellman_groups)
- [Encryption Algorithms](https://docs.strongswan.org/docs/5.9/config/IKEv2CipherSuites.html#_encryption_algorithms)
- [Integrity Algorithms](https://docs.strongswan.org/docs/5.9/config/IKEv2CipherSuites.html#_integrity_algorithms)
- [Weak Cryptographic Algorithms](https://docs.strongswan.org/docs/5.9/howtos/securityRecommendations.html#_weak_cryptographic_algorithms)
