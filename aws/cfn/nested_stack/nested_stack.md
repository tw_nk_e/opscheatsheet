# Nested Stack

<!-- vscode-markdown-toc -->
* 1. [Drift - BlockDeviceMappings](#Drift-BlockDeviceMappings)
	* 1.1. [Update VolumeSize](#UpdateVolumeSize)
	* 1.2. [Overrides VolumeSize](#OverridesVolumeSize)
	* 1.3. [Import VolumeSize](#ImportVolumeSize)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Drift-BlockDeviceMappings'></a>Drift - BlockDeviceMappings

<p>
  <u><b>Contexte :</b></u>
</p>

Une EC2 avec un *root volume* de 40 GiB a été déployé par une *nested stack* :
- *root stack* = [`cfn-sbx-nested-root.yml`](./src/cfn-sbx-nested-root.yml)
- *nested stack* = [`cfn-sbx-nested-ec2.yml`](./src/cfn-sbx-nested-ec2.yml)

1. Le *root volume* de l'EC2 a été édité manuellement (40 > 50 GiB) depuis AWS UI.

2. La taille du *root volume* de l'EC2 est définit dans la *root stack* `cfn-sbx-nested-root.yml` :
```yml
Parameters:
  InstanceRootSize:
    Type: Number
    Default: 40

Resources:
  EC2:
    Type: AWS::CloudFormation::Stack
    Properties:
      Parameters:
        EC2VolumeRootSize: !Ref InstanceRootSize
      TemplateURL: ./cfn-sbx-nested-ec2.yml
```

3. Nous avons un *drift* sur la stack `cfn-sbx-nested-root-EC2-...` :

| Property                             | Change    | Expected value | Current value |
| ------------------------------------ | --------- | -------------- | ------------- |
| BlockDeviceMappings.0.Ebs.VolumeSize | NOT_EQUAL | 40             | 50            |

###  1.1. <a name='UpdateVolumeSize'></a>Update VolumeSize

La mise à jours du paramètre `InstanceRootSize` de `40` à `50` ne fonctionnera pas. Lorsqu'on déploie une stack CFN pour la première fois, les valeurs `Default` sont utilisées pour définir la valeur des `Parameters` si aucune valeur n'est spécifiée explicitement. Cependant, une fois que la stack est créée, la valeur `Default` n'est plus pris en compte pour les mises à jour ultérieures.

1. Modification de la stack `cfn-sbx-nested-root.yml` :
```yml
Parameters:
  InstanceRootSize:
    Type: Number
    # Default: 40
    Default: 50
```

2. Génération d'un *change set* :
```json
{
	"Changes": [
		{
			"Type": "Resource",
			"ResourceChange": {
				"Action": "Modify",
				"LogicalResourceId": "EC2",
				"PhysicalResourceId": "arn:aws:cloudformation:eu-west-1:111111111111:stack/cfn-sbx-nested-root-EC2-.../563d4620-5dfd-11ef-9c19-069b6af52e17",
				"ResourceType": "AWS::CloudFormation::Stack",
				"Replacement": "False",
				"Scope": [
					"Properties"
				],
				"Details": [
					{
						"Target": {
							"Attribute": "Properties",
							"RequiresRecreation": "Never"
						},
						"Evaluation": "Dynamic",
						"ChangeSource": "Automatic"
					}
				]
			}
		}
	],
	...
	"StackName": "cfn-sbx-nested-root",
	...
	"Parameters": [
		...
		{
			"ParameterKey": "InstanceRootSize",
			"ParameterValue": "40"
		},
		...
	],
	...
	"ExecutionStatus": "AVAILABLE",
	"Status": "CREATE_COMPLETE",
	...
}
```
La nouvelle valeur `50` n'a pas été pris en compte (`{ "ParameterKey": "InstanceRootSize", "ParameterValue": "40" }`). Le *change set* peux être appliqué, il y aura aucun impacte sur la *nested stack*.

###  1.2. <a name='OverridesVolumeSize'></a>Overrides VolumeSize

Comme indiquer dans la documentation AWS ([AWS::EC2::Instance](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-ec2-instance.html#cfn-ec2-instance-blockdevicemappings)) il nous est impossible de modifier la taille des volumes déclarer dans `BlockDeviceMappings` une fois la machine livré sans délencher un remplacement de la ressource.

AWS propose la méthode suivante pour modifier un Root Volume d'une CFN sans remplacer l'EC2 : [How do I update my EBS volume in CloudFormation without EC2 instances being replaced?](https://repost.aws/knowledge-center/cloudformation-update-volume-instance). Pour résumer, nous devons passer par une importation : [Import an existing resource into a stack using the AWS CLI](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/resource-import-existing-stack.html#resource-import-existing-stack-cli).

###  1.3. <a name='ImportVolumeSize'></a>Import VolumeSize

Voici le plan d'action à appliquer pour importer une ressource "modifier manuellement" :
- Rajouter `DeletionPolicy: Retain` sur la STACK de la ressource cible.
- Pousser le nouveau template qui contenant `DeletionPolicy: Retain`.
- Supprimer la ressource à importer du template.
- Importer la ressource "sorti" du template avec les paramètres correspondant au modification faite manuellement.

Voici les étapes à appliquer pour corriger le *drift* de notre *nested stack* :

1. Modifier la *nested stack* `cfn-sbx-nested-ec2.yml` en y incluant `DeletionPolicy: Retain` sur notre ressource `Instance` :
```yml
Resources:
  Instance:
    Type: AWS::EC2::Instance
    DeletionPolicy: Retain
    ...
```

2. Pousser la modification depuis la *root stack* afin d'informer AWS CloudFormation de ne pas détruire la ressource `Instance` de la *nested stack* `cfn-sbx-nested-ec2.yml`.

3. Récupérer l'URL S3 du template de la *nested stack* `cfn-sbx-nested-ec2.yml` que vous venez de pousser. Cette URL doit contenir le template avec les modifications que vous venez de déployer. Ex: `https://cfn-sbx-nested-....s3.eu-west-1.amazonaws.com/nested/114334f805255ec53d8efd332354d2b2.template`.

4. Supprimer la ressource `Instance` de la *nested stack* `cfn-sbx-nested-ec2.yml` :
```yml
# Resources:
#   Instance:
#     Type: AWS::EC2::Instance
#     DeletionPolicy: Retain
#     ...
```

5. Pousser les modifications depuis la *root stack* afin que AWS CloudFormation supprime la ressource `Instance` de la *nested stack* `cfn-sbx-nested-root-EC2-...` sans la détruire (`DeletionPolicy: Retain`).

Events for `cfn-sbx-nested-root-EC2-...`
| Timestamp                    | Logical ID                            | Status                                                           | Detailed status | Status reason |
| ---------------------------- | ------------------------------------- | ---------------------------------------------------------------- | --------------- | ------------- |
| 2024-08-20 16:15:38 UTC+0200 | cfn-sbx-nested-root-EC2-...           | <font color="#1b7104">UPDATE_COMPLETE</font>                     | -               | -             |
| 2024-08-20 16:15:38 UTC+0200 | Instance                              | <font color="#0b5dad">DELETE_SKIPPED</font>                      | -               | -             |
| 2024-08-20 16:15:27 UTC+0200 | cfn-sbx-nested-root-EC2-... | <font color="#0b5dad">UPDATE_COMPLETE_CLEANUP_IN_PROGRESS</font> | -               | -             |
| 2024-08-20 16:15:24 UTC+0200 | cfn-sbx-nested-root-EC2-... | <font color="#0b5dad">UPDATE_IN_PROGRESS</font>                  | -               | -             |

6. Importer notre ressource `Instance` précédament sorti de la *nested stack* `cfn-sbx-nested-root-EC2-...`.
 
7. La commande `get-template-summary` permet de savoir quelles propriétés identifient chaque type de ressource dans le template. Utilisez l'URL du template de l'étape 3.
```sh
template_url='https://cfn-sbx-nested-....s3.eu-west-1.amazonaws.com/nested/114334f805255ec53d8efd332354d2b2.template' 

aws cloudformation get-template-summary \
    --template-url ${template_url} \
    --region eu-west-1 > get-template-summary.json
```
`get-template-summary.json` nous donne aussi l'ensemble des `Parameters` qu'on devra renseignez pour l'importation (les mêmes que ceux de la *nested stack* `cfn-sbx-nested-ec2.yml`)

8. Créer le fichier `resourcesToImport.json` :
```json
[
  {
    "ResourceType": "AWS::EC2::Instance",
    "LogicalResourceId": "Instance",
    "ResourceIdentifier": {
      "InstanceId": "i-03f3ba998caa24254"
    }
  }
]
```
On identifie la ressource que nous souhaitons importer avec l'id de la cible.

9. Créer le fichier `parameters.json` en présisant la nouvelle taille du `root volume` :
```json
[
  ...
  {
    "ParameterKey": "EC2VolumeRootSize",
    "ParameterValue": "50"
  },
  ...
]
```

10.  Pousser l'importation sur la *nested stack* `cfn-sbx-nested-root-EC2-...` :
```sh
template_url='https://cfn-sbx-nested-....s3.eu-west-1.amazonaws.com/nested/114334f805255ec53d8efd332354d2b2.template'
stack_name='cfn-sbx-nested-root-EC2-...'

aws cloudformation create-change-set \
    --stack-name ${stack_name} \
    --change-set-name ImportChangeSet \
    --change-set-type IMPORT \
    --template-url ${template_url} \
    --resources-to-import file://resourcesToImport.json \
    --parameters file://parameters.json \
    --region eu-west-1
```

11. Vérifier que le *change set* importe bien la ressource souhaitez.
```sh
stack_name='cfn-sbx-nested-root-EC2-...'

aws cloudformation describe-change-set \
    --change-set-name ImportChangeSet \
    --stack-name ${stack_name} \
    --region eu-west-1
```

12. Appliquer *change set* afin d'importer la ressource.
```sh
stack_name='cfn-sbx-nested-root-EC2-...'

aws cloudformation execute-change-set \
    --change-set-name ImportChangeSet \
    --stack-name ${stack_name} \
    --region eu-west-1
```

13.  Exécutez un *detect drift* sur la *nested stack* `cfn-sbx-nested-ec2.yml` pour confirmer que l'importation c'est bien déroulé.

14. Retirer les commentaire sur la ressource `Instance` de la *nested stack* `cfn-sbx-nested-ec2.yml` et si vous le souhaitez la protection `DeletionPolicy: Retain` :
```yml
Resources:
  Instance:
    Type: AWS::EC2::Instance
    DeletionPolicy: Retain
    ...
```

15.   Déployer la *root stack* en *overrides* le paramètre `InstanceRootSize` (`--parameter-overrides InstanceRootSize=50`). Vous pouvez le faire en deux fois avec un *change set* qui devrait ressembler à ca :

```json
{
	"Changes": [
		{
			"Type": "Resource",
			"ResourceChange": {
				"Action": "Modify",
				"LogicalResourceId": "EC2",
				"PhysicalResourceId": "arn:aws:cloudformation:eu-west-1:111111111111:stack/cfn-sbx-nested-root-EC2-.../cd0166a0-5f9d-11ef-80e6-06a432575d41",
				"ResourceType": "AWS::CloudFormation::Stack",
				"Replacement": "False",
				"Scope": [
					"Properties"
				],
				"Details": [
					{
						"Target": {
							"Attribute": "Properties",
							"Name": "Parameters",
							"RequiresRecreation": "Never"
						},
						"Evaluation": "Static",
						"ChangeSource": "ParameterReference",
						"CausingEntity": "InstanceRootSize"
					},
					{
						"Target": {
							"Attribute": "Properties",
							"Name": "TemplateURL",
							"RequiresRecreation": "Never"
						},
						"Evaluation": "Static",
						"ChangeSource": "DirectModification"
					},
					{
						"Target": {
							"Attribute": "Properties",
							"Name": "Parameters",
							"RequiresRecreation": "Never"
						},
						"Evaluation": "Dynamic",
						"ChangeSource": "DirectModification"
					}
				]
			}
		}
	],
	...
	"StackName": "cfn-sbx-nested-root",
	...
	"Parameters": [
		...
		{
			"ParameterKey": "InstanceRootSize",
			"ParameterValue": "50"
		},
		...
	],
	...
	"ExecutionStatus": "AVAILABLE",
	"Status": "CREATE_COMPLETE",
	...
}
```

