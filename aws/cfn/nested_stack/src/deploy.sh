#!/usr/bin/env bash

ROOT_STACK='cfn-sbx-nested-root'
S3_BUCKET=''

echo -e "\n*** CLOUDFORMATION PACKAGE ***\n"
aws cloudformation package \
  --template-file ${ROOT_STACK}.yml \
  --output-template ${ROOT_STACK}-pkg.yml \
  --s3-bucket ${S3_BUCKET} \
  --s3-prefix nested \
  --region eu-west-1
exit_status=$?

echo -e "\n*** CLOUDFORMATION DEPLOY ***\n"
aws cloudformation deploy \
  --template-file ${ROOT_STACK}-pkg.yml \
  --stack-name ${ROOT_STACK} \
  --s3-bucket ${S3_BUCKET} \
  --s3-prefix root \
  --region eu-west-1
exit_status=$?

if [ $exit_status -eq 255 ]; then
  aws cloudformation delete-stack \
    --stack-name ${ROOT_STACK} \
    --region eu-west-1
    echo -e ""
    echo -e "\n*** CLOUDFORMATION DELETE-STACK ***\n"
fi
