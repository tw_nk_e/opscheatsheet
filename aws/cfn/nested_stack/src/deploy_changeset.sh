#!/usr/bin/env bash

ROOT_STACK='cfn-sbx-nested-root'
S3_BUCKET=''
REGION_AWS='eu-west-1'

ask_question() {
  while true; do
    read -p "Execute change set? (y/n)" answer
    answer=$(echo "${answer}" | tr '[:upper:]' '[:lower:]')

    if [ "${answer}" = "y" ] || [ "${answer}" = "n" ]; then
      break
    else
      echo "Invalid answer. Please answer with 'y' or 'n'."
    fi
  done
}

echo -e "\n*** CLOUDFORMATION PACKAGE ***\n"
aws cloudformation package \
  --template-file ${ROOT_STACK}.yml \
  --output-template ${ROOT_STACK}-pkg.yml \
  --s3-bucket ${S3_BUCKET} \
  --s3-prefix nested \
  --region ${REGION_AWS}
exit_status=$?

echo -e "\n*** CLOUDFORMATION DEPLOY ***\n"
output=$(aws cloudformation deploy \
  --template-file ${ROOT_STACK}-pkg.yml \
  --stack-name ${ROOT_STACK} \
  --s3-bucket ${S3_BUCKET} \
  --s3-prefix root \
  --no-execute-changeset \
  --region ${REGION_AWS})
echo "${output}"
change_set_arn=$(echo "${output}" | ggrep -oP 'arn:aws:cloudformation:\S+')

echo -e "\n*** CLOUDFORMATION DESCRIBE-CHANGE-SET ***\n"
aws cloudformation describe-change-set \
  --change-set-name ${change_set_arn} \
  --region ${REGION_AWS}

ask_question
if [ "${answer}" = "y" ]; then
  echo -e "\n*** CLOUDFORMATION EXECUTE-CHANGE-SET ***\n"
  aws cloudformation execute-change-set \
    --change-set-name ${change_set_arn} \
    --region ${REGION_AWS}
fi
