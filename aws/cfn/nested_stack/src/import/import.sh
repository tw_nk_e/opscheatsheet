#!/usr/bin/env bash

template_url='https://cfn-sbx-nested-....s3.eu-west-1.amazonaws.com/nested/114334f805255ec53d8efd332354d2b2.template'
stack_name='cfn-sbx-nested-root-EC2-1FXLG9K5PFH6R'

echo -e "\n*** CLOUDFORMATION GET-TEMPLATE-SUMMARY ***\n"
aws cloudformation get-template-summary \
    --template-url ${template_url} \
    --region eu-west-1 > get-template-summary.json
exit_status=$?

echo -e "\n*** CLOUDFORMATION CREATE-CHANGE-SET ***\n"
aws cloudformation create-change-set \
    --stack-name ${stack_name} \
    --change-set-name ImportChangeSet \
    --change-set-type IMPORT \
    --template-url ${template_url} \
    --resources-to-import file://resourcesToImport.json \
    --parameters file://parameters.json \
    --region eu-west-1

echo -e "\n*** CLOUDFORMATION DESCRIBE-CHANGE-SET ***\n"
aws cloudformation describe-change-set \
    --change-set-name ImportChangeSet \
    --stack-name ${stack_name} \
    --region eu-west-1