# CloudFormation cheatsheet

<!-- vscode-markdown-toc -->
* 1. [Object of String](#ObjectofString)
* 2. [Array of Tag](#ArrayofTag)
* 3. [Array of String](#ArrayofString)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='ObjectofString'></a>Object of String

```yml
Tags
  Type: Object of String
```
YAML:
```yml
Tags:
  CloudFormation:Id: !Ref 'AWS::StackId'
  CloudFormation:Name: !Ref 'AWS::StackName'
```

##  2. <a name='ArrayofTag'></a>Array of Tag

```yml
Tags
  Type: Array of Tag
```

YAML:
```yml
Tags: 
  - Key: "CloudFormation:Id"
    Value: !Ref 'AWS::StackId'
  - Key: "CloudFormation:Name"
    Value: !Ref 'AWS::StackName'
```

##  3. <a name='ArrayofString'></a>Array of String

```yml
SubnetIds
  Type: Array of String
```

YAML:
```yml
SubnetIds:
  - subnet-11111111111111111
  - subnet-22222222222222222
```