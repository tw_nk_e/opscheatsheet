#!/usr/bin/env bash
set -e

readonly ACCOUNT_ID=$(aws sts get-caller-identity \
  --query 'Account' \
  --output text)

readonly ACCOUNT_ALIAS=$(aws iam list-account-aliases \
  --query 'AccountAliases' \
  --output text)

readonly REGION=$(aws configure get region)

ec2_id_list=$(aws ec2 describe-instances \
  --query 'Reservations[].Instances[?State.Name != `terminated`].InstanceId' \
  --output text | tr '\t' '\n')

ami_id_ec2_id_list=$(aws ec2 describe-images \
  --region "${REGION}" \
  --owner "${ACCOUNT_ID}" \
  --query 'reverse(sort_by(Images, &CreationDate))[].[ImageId, SourceInstanceId]' \
  --output text)

declare -A ami_ec2_map
declare -a ghost_ami_array

while IFS=$'\t' read -r ami_id ami_ec2_id; do
  ami_ec2_map["${ami_id}"]="${ami_ec2_id}"
done <<<"${ami_id_ec2_id_list}"

################################################################################

check_ec2_id_exists() {
  local ec2_id="$1"
  echo "${ec2_id_list}" | grep -qx "${ec2_id}"
}

################################################################################
echo -e "\n> INIT"
echo "Working on: ${ACCOUNT_ALIAS} (${ACCOUNT_ID})"

echo -e "\n> Find Ghost AMI 💥"
for ami in "${!ami_ec2_map[@]}"; do
  ami_ec2_id=${ami_ec2_map[${ami}]}
  if ! check_ec2_id_exists "${ami_ec2_id}"; then
    echo "Ghost AMI (${ami}), ${ami_ec2_id} don't exist."
    ghost_ami_array+=("${ami}")
  fi
done

echo -e "\n> Delete Ghost AMI"
for ami_id in "${ghost_ami_array[@]}"; do
  exit_code=0
  temp_log=$(mktemp -p /tmp)
  ami_snap_id=($(aws ec2 describe-images \
    --region "${REGION}" \
    --owner "${ACCOUNT_ID}" \
    --image-ids "${ami_id}" \
    --query 'Images[].[BlockDeviceMappings[].Ebs.SnapshotId]' \
    --output text))

  aws ec2 deregister-image \
    --image-id "${ami_id}" 2>"${temp_log}" || exit_code=$?
  if [ $exit_code -eq 0 ]; then
    :
  elif [ $exit_code -eq 254 ]; then
    message=$(tr -d '\n' <"${temp_log}")
    echo "${message}"
  else
    echo "${message}"
    exit ${exit_code}
  fi
  rm -f "${temp_log}"

  for snap_id in "${ami_snap_id[@]}"; do
    exit_code=0
    temp_log=$(mktemp -p /tmp)

    aws ec2 delete-snapshot \
      --snapshot-id ${snap_id} 2>"${temp_log}" || exit_code=$?
    if [ $exit_code -eq 0 ]; then
      :
    elif [ ${exit_code} -eq 254 ]; then
      message=$(tr -d '\n' <"${temp_log}")
      echo "${message}"
    else
      echo "${message}"
      exit ${exit_code}
    fi
    rm -f "${temp_log}"
  done

  if [ ${exit_code} -eq 0 ]; then
    echo "Ghost AMI (${ami_id}) are deleted with: ${ami_snap_id[@]}."
  else
    echo "Error: Ghost AMI (${ami_id}) are not deleted with: ${ami_snap_id[@]}."
  fi
done
