#!/usr/bin/env bash
set -e

readonly ACCOUNT_ID=$(aws sts get-caller-identity \
  --query 'Account' \
  --output text)

readonly ACCOUNT_ALIAS=$(aws iam list-account-aliases \
  --query 'AccountAliases' \
  --output text)

readonly REGION=$(aws configure get region)

ebs_id_list=$(aws ec2 describe-volumes \
  --query 'Volumes[].VolumeId' \
  --output text | tr '\t' '\n')

snapshot_id_ebs_id_list=$(aws ec2 describe-snapshots \
  --owner-ids "${ACCOUNT_ID}" \
  --query 'reverse(sort_by(Snapshots, &StartTime))[].[SnapshotId, VolumeId]' \
  --output text)

snapshot_id_ami_id_list=$(aws ec2 describe-snapshots \
  --owner-ids "${ACCOUNT_ID}" \
  --query 'reverse(sort_by(Snapshots, &StartTime)) [?starts_with(Description, `Created by CreateImage`)].[SnapshotId, Description]' \
  --output text | awk '{print $1 "\t" $NF}')

ami_id_list=$(aws ec2 describe-images \
  --region "${REGION}" \
  --owner "${ACCOUNT_ID}" \
  --query 'reverse(sort_by(Images, &CreationDate)) [].ImageId' \
  --output text | tr '\t' '\n')

declare -a ghost_snapshot_array
declare -A snapshot_ebs_map
declare -A snapshot_ami_map

while IFS=$'\t' read -r snapshot_id snap_ebs_id; do
  snapshot_ebs_map["${snapshot_id}"]="${snap_ebs_id}"
done <<<"${snapshot_id_ebs_id_list}"

while IFS=$'\t' read -r snapshot_id ami_id; do
  snapshot_ami_map["${snapshot_id}"]="${ami_id}"
done <<<"${snapshot_id_ami_id_list}"

################################################################################

check_ebs_id_exists() {
  local ebs_id="$1"
  echo "${ebs_id_list}" | grep -qx "${ebs_id}" # return 0 if match
}

check_ami_id_exists() {
  local ami_id="$1"
  echo "${ami_id_list}" | grep -qx "${ami_id}"
}

################################################################################
echo -e "\n> INIT"
echo "Working on: ${ACCOUNT_ALIAS} (${ACCOUNT_ID})"

echo -e "\n> Find Ghost Snapshot (from EBS) 💥"
for snapshot in "${!snapshot_ebs_map[@]}"; do
  snap_ebs_id=${snapshot_ebs_map[${snapshot}]}
  if ! check_ebs_id_exists "${snap_ebs_id}"; then
    echo "Ghost snapshot (${snapshot}), ${snap_ebs_id} don't exist."
    ghost_snapshot_array+=("${snapshot}")
  fi
done

echo -e "\n> Find Ghost Snapshot (from AMI) 💥"
for snapshot in "${!snapshot_ami_map[@]}"; do
  ami_id=${snapshot_ami_map[${snapshot}]}
  if ! check_ami_id_exists "${ami_id}"; then
    echo "Ghost snapshot (${snapshot}), ${ami_id} don't exist."
    ghost_snapshot_array+=("${snapshot}")
  fi
done

echo -e "\n> Delete Ghost Snapshot"
for snap_id in "${ghost_snapshot_array[@]}"; do
  exit_code=0
  temp_log=$(mktemp -p /tmp)

  aws ec2 delete-snapshot \
    --snapshot-id "${snap_id}" 2>"${temp_log}" || exit_code=$?
  if [ $exit_code -eq 0 ]; then
    :
  elif [ ${exit_code} -eq 254 ]; then
    message=$(tr -d '\n' <"${temp_log}")
    echo "${message}"
  else
    echo "${message}"
    exit ${exit_code}
  fi
  rm -f "${temp_log}"

  if [ ${exit_code} -eq 0 ]; then
    echo "Ghost snapshot (${snap_id}) are deleted."
  else
    echo "Error: Ghost snapshot (${snap_id}) are not deleted."
  fi
done
