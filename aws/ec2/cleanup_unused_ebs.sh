#!/usr/bin/env bash
set -e

readonly ACCOUNT_ID=$(aws sts get-caller-identity \
  --query 'Account' \
  --output text)

readonly ACCOUNT_ALIAS=$(aws iam list-account-aliases \
  --query 'AccountAliases' \
  --output text)

ebs_ids_unused_aray=($(aws ec2 describe-volumes \
  --query 'Volumes[?State == `available` && Attachments == `[]`].VolumeId' \
  --output text | tr '\t' '\n'))

echo -e "\n> INIT"
echo "Working on: ${ACCOUNT_ALIAS} (${ACCOUNT_ID})"

echo -e "\n> Find unused EBS & deleted 💥"
for ebs in "${ebs_ids_unused_aray[@]}"; do
  echo "${ebs} are unused and not attached."
  aws ec2 delete-volume --volume-id ${ebs}
  echo "Unused EBS ${ebs} are deleted."
done
