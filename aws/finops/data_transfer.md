# Data Transfer

<!-- vscode-markdown-toc -->
* 1. [Cost Explorer](#CostExplorer)
* 2. [Data Transfer "Internet"](#DataTransferInternet)
* 3. [Data Transfer "Region / InterRegion"](#DataTransferRegionInterRegion)
* 4. [Data Transfer "Across AZ / IntraRegion"](#DataTransferAcrossAZIntraRegion)
	* 4.1. [Not Free](#NotFree)
	* 4.2. [Free](#Free)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='CostExplorer'></a>Cost Explorer Analysis

Usage type "DataTransfer":
- [x] DataTransfer-In-Bytes
- [x] DataTransfer-Out-Bytes
- [x] EU-DataTransfer-In-Bytes
- [x] EU-DataTransfer-Out-Bytes
- [x] EU-DataTransfer-Regional-Bytes 
- [x] EU-DataTransfer-xAZ-In-Bytes
- [x] EU-DataTransfer-xAZ-Out-Bytes

##  2. <a name='DataTransferInternet'></a>Data Transfer "Internet"

> Region: Europe (Ireland)

Data Transfer OUT From Amazon EC2 To Internet [1] (Data transfer out to the internet [2]) 

*AWS customers receive 100GB of data transfer out to the internet free each month, aggregated across all AWS Services and Regions (except China and GovCloud). The 100 GB free tier for data transfer out to the internet is global and does not apply separately or individually to AWS Regions.*

  - First 10 TB / Month = $0.09 per GB
  - Next 40 TB / Month  = $0.085 per GB
  - Next 100 TB / Month = $0.07 per GB
  - Greater than 150 TB / Month = $0.05 per GB

**CUR / CostExplorer:**

UsageType: [2] [5]
- `<REGION>-DataTransfer-Out-Bytes` 
- `DataTransfer-Out-Bytes`

> Ex: EU-DataTransfer-Out-Bytes

**Bills**

```txt
Data Transfer
├── EU (Ireland)
│   ├── Bandwidth
│   │   └── $0.090 per GB - first 10 TB / month data transfer out beyond the global free tier
...
```

##  3. <a name='DataTransferRegionInterRegion'></a>Data Transfer "Region / InterRegion"

Data Transfer OUT From Amazon EC2 To [1] (Data transfer between AWS Regions [3])
  - ...
  - Europe (Frankfurt)	= $0.02 per GB
  - Europe (London)	    = $0.02 per GB
  - Europe (Milan)	    = $0.02 per GB
  - Europe (Paris)	    = $0.02 per GB
  - Europe (Spain)	    = $0.02 per GB
  - Europe (Stockholm)	= $0.02 per GB
  - Europe (Zurich)	    = $0.02 per GB
  - ...

**CUR / CostExplorer:**

UsageType: [3] [5]

- `<SRC_REGION>-<DST_REGION>-AWS-[In/Out]-Bytes` (InterRegion)
- `<SRC_REGION>-AWS-[In/Out]-Bytes` (Inter Region Peering)

> Ex: EU-EUC1-AWS-Out-Bytes, EU-USW2-AWS-Out-Bytes, ...

**Bills**

```txt
Data Transfer
├── EU (Ireland)
│   ├── AWS Data Transfer EU-APN2-AWS-Out-Bytes
│   │   └── $0.02 per GB - EU (Ireland) data transfer to Asia Pacific (Seoul)
│   ├── AWS Data Transfer EU-EUC1-AWS-Out-Bytes
│   │   └── $0.02 per GB - EU (Ireland) data transfer to EU (Germany)
...
```

##  4. <a name='DataTransferAcrossAZIntraRegion'></a>Data Transfer "Across AZ / IntraRegion"
###  4.1. <a name='NotFree'></a>Not Free

Data Transfer within the same AWS Region [1] (Data transfer within an AWS Region [4])

*Data transferred "in" to and "out" from Amazon EC2, Amazon RDS, Amazon Redshift, Amazon DynamoDB Accelerator (DAX), and Amazon ElastiCache instances, Elastic Network Interfaces or VPC Peering connections across Availability Zones in the same AWS Region is charged at $0.01/GB in each direction.*

*IPv4: Data transferred "in" to and "out" from public or Elastic IPv4 address is charged at $0.01/GB in each direction.*

*IPv6: Data transferred "in" to and "out" from an IPv6 address in a different VPC is charged at $0.01/GB in each direction.*

**CUR / CostExplorer:**

UsageType: [4] [5]
- `<REGION>-DataTransfer-Regional-Bytes`
  - Region / IntraRegion
  - Region / IntraRegion-VPCpeering
  - Load Balancers / IntraRegion
- `DataTransfer-Regional-Bytes`
  - Region / IntraRegion
  - Region / IntraRegion-VPCpeering
  - Load Balancers / IntraRegion

> Ex: EU-DataTransfer-Regional-Bytes

**Bills**

```txt
Data Transfer
├── EU (Ireland)
│   ├── Bandwidth
│   │   ├──$0.010 per GB - regional data transfer - in/out/between EC2 AZs or using elastic IPs or ELB
...
```

###  4.2. <a name='Free'></a>Free

Data Transfer across Availability Zones in the same AWS Region to or from VPC Endpoint, Transit Gateway, Client VPN.

**CUR / CostExplorer:**

UsageType: `<REGION>-DataTransfer-xAZ-[In/Out]-Bytes` [5]

---

SRC:

- [1] https://aws.amazon.com/ec2/pricing/on-demand/#Data_Transfer
- [2] https://docs.aws.amazon.com/cur/latest/userguide/cur-data-transfers-charges.html#data-transfer-out-internet
- [3] https://docs.aws.amazon.com/cur/latest/userguide/cur-data-transfers-charges.html#data-transfer-between-regions
- [4] https://docs.aws.amazon.com/cur/latest/userguide/cur-data-transfers-charges.html#data-transfer-within-region
- [5] https://aws.amazon.com/fr/blogs/networking-and-content-delivery/understand-aws-data-transfer-details-in-depth-from-cost-and-usage-report-using-athena-query-and-quicksight/
- https://aws.amazon.com/fr/blogs/architecture/overview-of-data-transfer-costs-for-common-architectures/
