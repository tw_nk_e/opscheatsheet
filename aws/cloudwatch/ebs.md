# EBS Metric

<!-- vscode-markdown-toc -->
* 1. [Introduction](#Introduction)
* 2. [Stress Test](#StressTest)
* 3. [CloudWatch](#CloudWatch)
	* 3.1. [AVERAGE IOPS](#AVERAGEIOPS)
	* 3.2. [AVERAGE READ IOPS](#AVERAGEREADIOPS)
	* 3.3. [AVERAGE WRITE IOPS](#AVERAGEWRITEIOPS)
	* 3.4. [AVERAGE ACTUAL THROUGHPUT (BYTE/S)](#AVERAGEACTUALTHROUGHPUTBYTES)
	* 3.5. [AVERAGE THROUGHPUT (BYTE/S)](#AVERAGETHROUGHPUTBYTES)
	* 3.6. [AVERAGE READ THROUGHPUT (BYTE/S)](#AVERAGEREADTHROUGHPUTBYTES)
	* 3.7. [AVERAGE WRITE THROUGHPUT (BYTE/S)](#AVERAGEWRITETHROUGHPUTBYTES)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Introduction'></a>Introduction

Les IOPS (*Input/Output Operations Per Second*), sont une mesure de la performance des dispositifs de stockage, notamment des disques SSD (*Solid State Drives*) et HDD (*Hard Disk Drives*). Les IOPS indiquent le nombre d'opérations de lecture ou d'écriture qu'un dispositif de stockage peut effectuer par seconde.

Le *block size* (par exemple, 4 KiB, 8 KiB, etc.) peut affecter le nombre total d'IOPS qu'un dispositif peut atteindre, car des blocs plus petits peuvent permettre plus d'opérations par seconde.

- Un dispositif de stockage capable de 10 000 IOPS avec des blocs de 4 KiB pourrait transférer 40 MiB de données par seconde (10 000 opérations * 4 KiB par opération).
- Le même dispositif avec des blocs de 8 KiB pourrait transférer 80 MiB de données par seconde (10 000 opérations * 8 KiB par opération), mais le nombre d'IOPS resterait le même.

Quelques points clés :

- IOPS : Mesure le nombre d'opérations de lecture/écriture par seconde.
- *Block size* : (par exemple, 4 KiB) peut influencer le nombre d'IOPS. Des blocs plus petits permettent généralement plus d'opérations par seconde.
- Throughput : est une autre mesure de performance qui indique la quantité de données transférées par seconde (par exemple, en MiB/s).

A noter pour plus tard :
- 1 MB (mégabytes)  = 1 000 000 bytes (octets) - doc non technique.
- 1 MiB (mégaoctet) = 1 048 576 bytes (octets) - doc technique calculée sur une base binaire.

##  2. <a name='StressTest'></a>Stress Test

1. Installer `fio` sur votre machine qui est beaucoup plus précis que `dd`.
2. Créer le fichier `stress_test.fio`.
  ```bash
  [global]
  ioengine=libaio
  direct=1
  rw=randrw
  bs=4k
  size=1G
  numjobs=1
  runtime=600
  group_reporting
  allow_mounted_write=1

  [job1]
  filename=/dev/nvme1n1
  rwmixread=70
  ```
  - `ioengine=libaio` : Utilise le moteur d'I/O asynchrone.
  - `direct=1` : Utilise l'I/O directe (O_DIRECT).
  - `rw=randrw` : Effectue des opérations de lecture/écriture aléatoires.
  - `bs=4k` : Taille des blocs de 4 KiB.
  - `size=1G` : Taille du fichier de test de 1 GiB.
  - `numjobs=1` : Nombre de travaux à exécuter en parallèle.
  - `runtime=600` : Durée du test en secondes.
  - `group_reporting` : Rapports groupés.
  - `allow_mounted_write=1` : Permet d'effectuer le test sur un volume monté. ATTENTION cela peux corrompre les données. Il est recomandé de démonté le disque.
  - `rwmixread=70` : Proportion de lectures (70%) par rapport aux écritures (30%).
  - `filename=/dev/nvme1n1` : Chemin vers le volume EBS GP3.
3. Exécutez la commande `sudo fio stress_test.fio`

Benchmark :
```bash
# Volume GP3 - 3 000 IOPS - Throughput 125 MiB/s.
job1: (g=0): rw=randrw, bs=(R) 4096B-4096B, (W) 4096B-4096B, (T) 4096B-4096B, ioengine=libaio, iodepth=1
fio-3.32
Starting 1 process
Jobs: 1 (f=1): [m(1)][100.0%][r=4000KiB/s,w=1585KiB/s][r=1000,w=396 IOPS][eta 00m:00s]
job1: (groupid=0, jobs=1): err= 0: pid=26687: Thu Jan 23 09:26:40 2025
  read: IOPS=975, BW=3901KiB/s (3994kB/s)(229MiB/60001msec)
    ...
  write: IOPS=416, BW=1665KiB/s (1705kB/s)(97.6MiB/60001msec); 0 zone resets
    ...
  ...

Run status group 0 (all jobs):
   READ: bw=3901KiB/s (3994kB/s), 3901KiB/s-3901KiB/s (3994kB/s-3994kB/s), io=229MiB (240MB), run=60001-60001msec
  WRITE: bw=1665KiB/s (1705kB/s), 1665KiB/s-1665KiB/s (1705kB/s-1705kB/s), io=97.6MiB (102MB), run=60001-60001msec

Disk stats (read/write):
  nvme1n1: ios=58415/24935, merge=0/0, ticks=35165/22425, in_queue=57591, util=95.64%

# IOPS : READ=975 IOPS | WRITE=416 IOPS
# Throughput : READ=3901 KiB/s (3.9 MiB/s) | WRITE= 1665 KiB/s (1.6 MiB/s)
```

Augmenter le *block size* pour avoir stresser le *throughput*. Augmenter la taille du fichier, le nombre de jobs (et la durée) pour stresser les IOPS (`size=5G numjobs=15`).

##  3. <a name='CloudWatch'></a>CloudWatch

###  3.1. <a name='AVERAGEIOPS'></a>AVERAGE IOPS

Formule : `(SUM(VolumeReadOps) + SUM(VolumeWriteOps)) / (PERIOD(SUM(VolumeIdleTime)))`

- m1 = VolumeReadOps / Sum / 1 minute
- m2 = VolumeWriteOps / Sum / 1 minute
- m3 = VolumeIdleTime / Sum / 1 minute
- e1 = `AverageIops / (m1+m2)/(PERIOD(m3))`
- Horizontal annotations: Max IOPS - 3000

###  3.2. <a name='AVERAGEREADIOPS'></a>AVERAGE READ IOPS

Formule : `SUM(VolumeReadOps) / PERIOD(SUM(VolumeIdleTime))`

- m1 = VolumeReadOps / Sum / 1 minute
- m2 = VolumeIdleTime / Sum / 1 minute
- e1 = `m1/PERIOD(m2)`
- Horizontal annotations: Max IOPS - 3000

###  3.3. <a name='AVERAGEWRITEIOPS'></a>AVERAGE WRITE IOPS

Formule : `SUM(VolumeWriteOps) / PERIOD(SUM(VolumeIdleTime))`

- m1 = VolumeWriteOps / Sum / 1 minute
- m2 = VolumeIdleTime / Sum / 1 minute
- e1 = `m1/PERIOD(m2)`
- Horizontal annotations: Max IOPS - 3000

###  3.4. <a name='AVERAGEACTUALTHROUGHPUTBYTES'></a>AVERAGE ACTUAL THROUGHPUT (BYTE/S)

> Mesure le débit moyen lorsque le volume est actif.

Formule : `(SUM(VolumeReadBytes) + VolumeWriteBytes) / (PERIOD(SUM(VolumeReadBytes)) - SUM(VolumeIdleTime))`

- m1 = VolumeReadBytes / Sum / 1 minutes
- m2 = VolumeWriteBytes / Sum / 1 minutes
- m3 = VolumeIdleTime / Sum / 1 minutes
- e1 = `IF((PERIOD(m1)-m3)>0, (m1+m2)/(PERIOD(m1)-m3), 0)`
- Horizontal annotations: Max Throughput (125 MiB/s) - 131072000

Le `IF` permet de corriger la formule afin de dévisé des nombres négatifs (`IF(condition, trueValue, falseValue)`).

###  3.5. <a name='AVERAGETHROUGHPUTBYTES'></a>AVERAGE THROUGHPUT (BYTE/S)

> Mesure le débit moyen sur toute la période, y compris les périodes d'inactivité.

Formule : `(SUM(VolumeReadBytes) + SUM(VolumeWriteBytes)) / (PERIOD(SUM(VolumeIdleTime)))`

- m1 = VolumeReadBytes / Sum / 1 minutes
- m2 = VolumeWriteBytes / Sum / 1 minutes
- m3 = VolumeIdleTime / Sum / 1 minutes
- e1 = `(m1+m2)/(PERIOD(m3))`
- Horizontal annotations: Max Throughput (125 MiB/s) - 131072000

###  3.6. <a name='AVERAGEREADTHROUGHPUTBYTES'></a>AVERAGE READ THROUGHPUT (BYTE/S)

Formule : `SUM(VolumeReadBytes) / PERIOD(SUM(VolumeIdleTime))`

- m1 = VolumeReadBytes / Sum / 1 minutes
- m2 = VolumeIdleTime / Sum / 1 minutes
- e1 = `m1/PERIOD(m2)`
- Horizontal annotations: Max Throughput (125 MiB/s) - 131072000

###  3.7. <a name='AVERAGEWRITETHROUGHPUTBYTES'></a>AVERAGE WRITE THROUGHPUT (BYTE/S)

Formule : `SUM(VolumeWriteBytes) / PERIOD(SUM(VolumeIdleTime))`

- m1 = VolumeWriteBytes / Sum / 1 minutes
- m2 = VolumeIdleTime / Sum / 1 minutes
- e1 = `m1/PERIOD(m2)`
- Horizontal annotations: Max Throughput (125 MiB/s) - 131072000

---
SRC :
- https://repost.aws/knowledge-center/ebs-identify-micro-bursting
- https://aws.amazon.com/fr/blogs/storage/valuable-tips-for-monitoring-and-understanding-amazon-ebs-performance-using-amazon-cloudwatch/
- https://repost.aws/knowledge-center/ebs-cloudwatch-metrics-throughput-iops
- https://chat.mistral.ai
