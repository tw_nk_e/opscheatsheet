# Gestions des utilisateurs et des rôles de PostgreSQL

<!-- vscode-markdown-toc -->
* 1. [Création DB + usr](#CrationDBusr)
* 2. [Création d'une table + données](#Crationdunetabledonnes)
* 3. [L'inconvéniant du schéma public](#Linconvniantduschmapublic)
	* 3.1. [REVOKE ALL ON DATABASE](#REVOKEALLONDATABASE)
	* 3.2. [REVOKE CREATE ON SCHEMA](#REVOKECREATEONSCHEMA)
	* 3.3. [Création du role "Read-only"](#CrationduroleRead-only)
	* 3.4. [Création d'un utilisateur membre de "ReadOnly"](#CrationdunutilisateurmembredeReadOnly)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

> POC Info: Engine = psql (12.15, server 12.12)

##  1. <a name='CrationDBusr'></a>Création DB + usr

Template :
```sql
CREATE ROLE "<usr_name>" WITH
  -- NOSUPERUSER
  -- NOCREATEDB
  -- NOCREATEROLE
  NOINHERIT -- role does not automatically inherit privileges from parent roles
  LOGIN
  -- NOREPLICATION
  -- NOBYPASSRLS
  -- CONNECTION LIMIT -1
  ENCRYPTED PASSWORD 'azerty';
CREATE DATABASE "<db_name>"
  WITH
  ENCODING = "UTF8"
  CONNECTION LIMIT = -1
  IS_TEMPLATE = False;
ALTER DATABASE "<db_name>" OWNER TO "<usr_name>";
```

Exemple :
```sql
postgres=> \du+
                                                                      List of roles
    Role name    |                         Attributes                         |                          Member of                          | Description 
-----------------+------------------------------------------------------------+-------------------------------------------------------------+-------------
 database_dev    | No inheritance                                             | {}                                                          | 
 database_int    | No inheritance                                             | {}                                                          | 
 postgres        | Create role, Create DB                                    +| {rds_superuser}                                             | 
                 | Password valid until infinity                              |                                                             | 
 rds_ad          | Cannot login                                               | {}                                                          | 
 rds_iam         | Cannot login                                               | {}                                                          | 
 rds_password    | Cannot login                                               | {}                                                          | 
 rds_replication | Cannot login                                               | {}                                                          | 
 rds_superuser   | Cannot login                                               | {pg_monitor,pg_signal_backend,rds_replication,rds_password} | 
 rdsadmin        | Superuser, Create role, Create DB, Replication, Bypass RLS+| {}                                                          | 
                 | Password valid until infinity                              |                                                             | 

postgres=> \l+
                                                                        List of databases
     Name     |    Owner     | Encoding |   Collate   |    Ctype    |   Access privileges   |   Size    | Tablespace |                Description                 
--------------+--------------+----------+-------------+-------------+-----------------------+-----------+------------+--------------------------------------------
 database_dev | database_dev | UTF8     | en_US.UTF-8 | en_US.UTF-8 |                       | 8105 kB   | pg_default | 
 database_int | database_int | UTF8     | en_US.UTF-8 | en_US.UTF-8 |                       | 8105 kB   | pg_default | 
 postgres     | postgres     | UTF8     | en_US.UTF-8 | en_US.UTF-8 |                       | 8396 kB   | pg_default | default administrative connection database
 rdsadmin     | rdsadmin     | UTF8     | en_US.UTF-8 | en_US.UTF-8 | rdsadmin=CTc/rdsadmin | No Access | pg_default | 
 template0    | rdsadmin     | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/rdsadmin          +| 8105 kB   | pg_default | unmodifiable empty database
              |              |          |             |             | rdsadmin=CTc/rdsadmin |           |            | 
 template1    | postgres     | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/postgres          +| 8251 kB   | pg_default | default template for new databases
              |              |          |             |             | postgres=CTc/postgres |           |            | 
(6 rows)
```

##  2. <a name='Crationdunetabledonnes'></a>Création d'une table + données

Template :
```sql
CREATE TABLE employees (
    id SERIAL PRIMARY KEY,
    name VARCHAR(100),
    age INTEGER,
    salary DECIMAL(10, 2)
);

INSERT INTO employees (name, age, salary) VALUES
    ('John Doe', 30, 5000.00),
    ('Jane Smith', 25, 4500.00),
    ('Bob Johnson', 35, 6000.00);

CREATE TABLE departments (
    id SERIAL PRIMARY KEY,
    name VARCHAR(100),
    location VARCHAR(100)
);

INSERT INTO departments (name, location) VALUES
    ('IT', 'New York'),
    ('HR', 'London'),
    ('Finance', 'Tokyo');

CREATE TABLE projects (
    id SERIAL PRIMARY KEY,
    name VARCHAR(100),
    department_id INTEGER,
    start_date DATE,
    end_date DATE
);

INSERT INTO projects (name, department_id, start_date, end_date) VALUES
    ('Project A', 1, '2022-01-01', '2022-06-30'),
    ('Project B', 2, '2022-03-15', '2022-09-30'),
    ('Project C', 1, '2022-02-01', '2022-08-31');
```

Exemple :
```sql
database_int=> \conninfo 
You are connected to database "database_int" as user "database_int" on host "...eu-west-1.rds.amazonaws.com" (address "x.x.x.x") at port "5432".
SSL connection (protocol: TLSv1.2, cipher: AES128-SHA256, bits: 128, compression: off)

database_int=> \dt+
                           List of relations
 Schema |    Name     | Type  |    Owner     |    Size    | Description 
--------+-------------+-------+--------------+------------+-------------
 public | departments | table | database_int | 8192 bytes | 
 public | employees   | table | database_int | 8192 bytes | 
 public | projects    | table | database_int | 8192 bytes | 
(3 rows)

database_int=> SELECT * FROM departments;
 id |  name   | location 
----+---------+----------
  1 | IT      | New York
  2 | HR      | London
  3 | Finance | Tokyo
(3 rows)
```

##  3. <a name='Linconvniantduschmapublic'></a>L'inconvéniant du schéma public

Lorsqu'une nouvelle DB est créée, PostgreSQL crée par défaut un schéma nommé `public` et accorde l'accès à ce schéma à un rôle backend nommé `public`. Tous les nouveaux utilisateurs et rôles se voient attribuer par défaut ce rôle `public`, et peuvent donc créer des objets dans le schéma `public`.  

Ici dans notre exemple nous pouvons donc :
- connecte to database "database_dev" with user "database_int" --> OK
- CREATE TABLE to database "database_dev" with user "database_int" --> OK
- connecte to database "database_int" with user "database_dev" --> OK
- CREATE TABLE to database "database_int" with user "database_dev" --> OK

> Par défaut, tout le monde dispose des privilèges CREATE et USAGE sur le schéma public. [^1]

Il existe une commande psql qui nous permet de vérifier ça en étant connecter sur la db cible (t = true; f = false) :
```sql
SELECT
  r.rolname AS role_name,
  current_database() AS database_name,
  has_database_privilege(current_database(), current_database(), 'CONNECT') AS connect_privilege,
  has_database_privilege(current_database(), current_database(), 'CREATE') AS create_privilege,
  has_database_privilege(current_database(), current_database(), 'TEMPORARY') AS temporary_privilege,
  has_schema_privilege(r.rolname, 'public', 'USAGE') AS schema_usage_privilege,
  has_schema_privilege(r.rolname, 'public', 'CREATE') AS schema_create_privilege
FROM pg_roles r
WHERE r.rolname <> 'rds_superuser' AND r.rolname NOT LIKE 'pg_%'
ORDER BY r.rolname;
```

Exemple : 
```sql
database_dev=> \conninfo 
You are connected to database "database_dev" as user "database_int" on host "...eu-west-1.rds.amazonaws.com" (address "x.x.x.x") at port "5432".
SSL connection (protocol: TLSv1.2, cipher: AES128-SHA256, bits: 128, compression: off)

database_dev=> SELECT
  r.rolname AS role_name,
  current_database() AS database_name,
  has_database_privilege(current_database(), current_database(), 'CONNECT') AS connect_privilege,
  has_database_privilege(current_database(), current_database(), 'CREATE') AS create_privilege,
  has_database_privilege(current_database(), current_database(), 'TEMPORARY') AS temporary_privilege,
  has_schema_privilege(r.rolname, 'public', 'USAGE') AS schema_usage_privilege,
  has_schema_privilege(r.rolname, 'public', 'CREATE') AS schema_create_privilege
FROM pg_roles r
WHERE r.rolname <> 'rds_superuser' AND r.rolname NOT LIKE 'pg_%'
ORDER BY r.rolname;
    role_name    | database_name | connect_privilege | create_privilege | temporary_privilege | schema_usage_privilege | schema_create_privilege 
-----------------+---------------+-------------------+------------------+---------------------+------------------------+-------------------------
 database_dev    | database_dev  | t                 | t                | t                   | t                      | t
 database_int    | database_dev  | t                 | t                | t                   | t                      | t
 postgres        | database_dev  | t                 | t                | t                   | t                      | t
 rds_ad          | database_dev  | t                 | t                | t                   | t                      | t
 rds_iam         | database_dev  | t                 | t                | t                   | t                      | t
 rds_password    | database_dev  | t                 | t                | t                   | t                      | t
 rds_replication | database_dev  | t                 | t                | t                   | t                      | t
 rdsadmin        | database_dev  | t                 | t                | t                   | t                      | t
(8 rows)
```

###  3.1. <a name='REVOKEALLONDATABASE'></a>REVOKE ALL ON DATABASE

La première étape consiste à autoriser uniquement le propriétaire de la DB à se connecter à ça propre DB.

> A noter, qu'il ne faut rien toucher au niveau de la DB `template0`.  
> La DB `template1` est similaire à la DB `template0` mais il est possible d'y ajouter des objets (tables, vues, fonctions, etc.) ou des données de configuration spécifiques qui seront automatiquement copiés dans les nouvelles bases de données créées à partir de `template1`.  
> Lorsque qu'on créer une nouvelle DB dans PostgreSQL, vous pouvez spécifier la DB à utiliser comme modèle en utilisant l'option `TEMPLATE`. Par défaut, si vous ne spécifiez pas de modèle, PostgreSQL utilise `template1`

```sql
REVOKE ALL ON DATABASE <db_target> FROM PUBLIC;
```

Exemple :
```sql
postgres=> \conninfo 
You are connected to database "postgres" as user "postgres" on host "...eu-west-1.rds.amazonaws.com" (address "x.x.x.x") at port "5432".
SSL connection (protocol: TLSv1.2, cipher: AES128-SHA256, bits: 128, compression: off)

postgres=> \du postgres
                        List of roles
 Role name |          Attributes           |    Member of    
-----------+-------------------------------+-----------------
 postgres  | Create role, Create DB       +| {rds_superuser}
           | Password valid until infinity |

postgres=> \l
                                     List of databases
     Name     |    Owner     | Encoding |   Collate   |    Ctype    |   Access privileges   
--------------+--------------+----------+-------------+-------------+-----------------------
 database_dev | database_dev | UTF8     | en_US.UTF-8 | en_US.UTF-8 | 
 database_int | database_int | UTF8     | en_US.UTF-8 | en_US.UTF-8 | 
 postgres     | postgres     | UTF8     | en_US.UTF-8 | en_US.UTF-8 | 
 rdsadmin     | rdsadmin     | UTF8     | en_US.UTF-8 | en_US.UTF-8 | rdsadmin=CTc/rdsadmin
 template0    | rdsadmin     | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/rdsadmin          +
              |              |          |             |             | rdsadmin=CTc/rdsadmin
 template1    | postgres     | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/postgres          +
              |              |          |             |             | postgres=CTc/postgres
(6 rows)

postgres=> REVOKE ALL ON DATABASE database_dev FROM PUBLIC;
REVOKE ALL ON DATABASE database_int FROM PUBLIC;
REVOKE ALL ON DATABASE postgres FROM PUBLIC;
REVOKE ALL ON DATABASE rdsadmin FROM PUBLIC;
REVOKE ALL ON DATABASE template1 FROM PUBLIC;
REVOKE
REVOKE
REVOKE
REVOKE
REVOKE
```

###  3.2. <a name='REVOKECREATEONSCHEMA'></a>REVOKE CREATE ON SCHEMA

La seconde étape consiste à révoquer le privilège de création (CREATE) sur le schéma "public" pour le rôle "PUBLIC" pour toutes les DB. La commande doit être exécuter sur chaque DB cible sauf les DB système (postgres, rdsadmin, template0, template1).  

**⚠️ ATTENTION ⚠️  
Le propriétaire de la DB ce voit aussi révoquer le privilège de création sur le schéma publique !!!**

Template :
```sql
REVOKE CREATE ON SCHEMA public FROM PUBLIC;
GRANT CREATE ON SCHEMA public TO <db_owner>;
```

Exemple : 
```sql
postgres=> \du postgres
                        List of roles
 Role name |          Attributes           |    Member of    
-----------+-------------------------------+-----------------
 postgres  | Create role, Create DB       +| {rds_superuser}
           | Password valid until infinity | 

database_dev=> \l+ database_dev
                                                            List of databases
     Name     |    Owner     | Encoding |   Collate   |    Ctype    |       Access privileges       |  Size   | Tablespace | Description 
--------------+--------------+----------+-------------+-------------+-------------------------------+---------+------------+-------------
 database_dev | database_dev | UTF8     | en_US.UTF-8 | en_US.UTF-8 | database_dev=CTc/database_dev | 8411 kB | pg_default | 
(1 row)

postgres=> \c database_dev postgres
Password for user database_dev: 
psql (12.15, server 12.12)
SSL connection (protocol: TLSv1.2, cipher: AES128-SHA256, bits: 128, compression: off)
You are now connected to database "database_dev" as user "database_dev".

database_dev=> SELECT
  r.rolname AS role_name,
  current_database() AS database_name,
  has_database_privilege(current_database(), current_database(), 'CONNECT') AS connect_privilege,
  has_database_privilege(current_database(), current_database(), 'CREATE') AS create_privilege,
  has_database_privilege(current_database(), current_database(), 'TEMPORARY') AS temporary_privilege,
  has_schema_privilege(r.rolname, 'public', 'USAGE') AS schema_usage_privilege,
  has_schema_privilege(r.rolname, 'public', 'CREATE') AS schema_create_privilege
FROM pg_roles r
WHERE r.rolname <> 'rds_superuser' AND r.rolname NOT LIKE 'pg_%'
ORDER BY r.rolname;
    role_name    | database_name | connect_privilege | create_privilege | temporary_privilege | schema_usage_privilege | schema_create_privilege 
-----------------+---------------+-------------------+------------------+---------------------+------------------------+-------------------------
 database_dev    | database_dev  | t                 | t                | t                   | t                      | t
 database_int    | database_dev  | t                 | t                | t                   | t                      | t
 postgres        | database_dev  | t                 | t                | t                   | t                      | t
 rds_ad          | database_dev  | t                 | t                | t                   | t                      | t
 rds_iam         | database_dev  | t                 | t                | t                   | t                      | t
 rds_password    | database_dev  | t                 | t                | t                   | t                      | t
 rds_replication | database_dev  | t                 | t                | t                   | t                      | t
 rdsadmin        | database_dev  | t                 | t                | t                   | t                      | t
(8 rows)

database_dev=> REVOKE CREATE ON SCHEMA public FROM PUBLIC;
REVOKE

database_dev=> SELECT                                     
  r.rolname AS role_name,
  current_database() AS database_name,
  has_database_privilege(current_database(), current_database(), 'CONNECT') AS connect_privilege,
  has_database_privilege(current_database(), current_database(), 'CREATE') AS create_privilege,
  has_database_privilege(current_database(), current_database(), 'TEMPORARY') AS temporary_privilege,
  has_schema_privilege(r.rolname, 'public', 'USAGE') AS schema_usage_privilege,
  has_schema_privilege(r.rolname, 'public', 'CREATE') AS schema_create_privilege
FROM pg_roles r
WHERE r.rolname <> 'rds_superuser' AND r.rolname NOT LIKE 'pg_%'
ORDER BY r.rolname;
    role_name    | database_name | connect_privilege | create_privilege | temporary_privilege | schema_usage_privilege | schema_create_privilege 
-----------------+---------------+-------------------+------------------+---------------------+------------------------+-------------------------
 database_dev    | database_dev  | t                 | t                | t                   | t                      | f
 database_int    | database_dev  | t                 | t                | t                   | t                      | f
 postgres        | database_dev  | t                 | t                | t                   | t                      | t
 rds_ad          | database_dev  | t                 | t                | t                   | t                      | f
 rds_iam         | database_dev  | t                 | t                | t                   | t                      | f
 rds_password    | database_dev  | t                 | t                | t                   | t                      | f
 rds_replication | database_dev  | t                 | t                | t                   | t                      | f
 rdsadmin        | database_dev  | t                 | t                | t                   | t                      | t
(8 rows)

database_dev=> GRANT CREATE ON SCHEMA public TO database_dev;
GRANT
database_dev=> SELECT
  r.rolname AS role_name,
  current_database() AS database_name,
  has_database_privilege(current_database(), current_database(), 'CONNECT') AS connect_privilege,
  has_database_privilege(current_database(), current_database(), 'CREATE') AS create_privilege,
  has_database_privilege(current_database(), current_database(), 'TEMPORARY') AS temporary_privilege,
  has_schema_privilege(r.rolname, 'public', 'USAGE') AS schema_usage_privilege,
  has_schema_privilege(r.rolname, 'public', 'CREATE') AS schema_create_privilege
FROM pg_roles r
WHERE r.rolname <> 'rds_superuser' AND r.rolname NOT LIKE 'pg_%'
ORDER BY r.rolname;
    role_name    | database_name | connect_privilege | create_privilege | temporary_privilege | schema_usage_privilege | schema_create_privilege 
-----------------+---------------+-------------------+------------------+---------------------+------------------------+-------------------------
 database_dev    | database_dev  | t                 | t                | t                   | t                      | t
 database_int    | database_dev  | t                 | t                | t                   | t                      | f
 postgres        | database_dev  | t                 | t                | t                   | t                      | t
 rds_ad          | database_dev  | t                 | t                | t                   | t                      | f
 rds_iam         | database_dev  | t                 | t                | t                   | t                      | f
 rds_password    | database_dev  | t                 | t                | t                   | t                      | f
 rds_replication | database_dev  | t                 | t                | t                   | t                      | f
 rdsadmin        | database_dev  | t                 | t                | t                   | t                      | t
(8 rows)
```

###  3.3. <a name='CrationduroleRead-only'></a>Création du role "Read-only"

Maintenant que nous sommes "propres" nous allons pouvoir créer notre rôle. La bonne pratique veut que créons un rôle "ReadOnly" avec un certain nombre de privilège. Ensuite nous pourrons créer un utilisateur afin qu'il soit membre du rôle "ReadOnly".

1. Création du role

Template :
```sql
CREATE ROLE "rl_readonly" WITH
  -- NOSUPERUSER
  -- NOCREATEDB
  -- NOCREATEROLE
  NOINHERIT -- role does not automatically inherit privileges from parent roles
  -- NOLOGIN
  -- NOREPLICATION
  -- NOBYPASSRLS
  -- CONNECTION LIMIT -1
  -- PASSWORD NULL
  ;
```

Exemple : 
```sql
postgres=> \conninfo 
You are connected to database "postgres" as user "postgres" on host "...eu-west-1.rds.amazonaws.com" (address "x.x.x.x") at port "5432".
SSL connection (protocol: TLSv1.2, cipher: AES128-SHA256, bits: 128, compression: off)

postgres=> \du postgres
                        List of roles
 Role name |          Attributes           |    Member of    
-----------+-------------------------------+-----------------
 postgres  | Create role, Create DB       +| {rds_superuser}
           | Password valid until infinity | 

postgres=> CREATE ROLE "rl_readonly" WITH
  -- NOSUPERUSER
  -- NOCREATEDB
  -- NOCREATEROLE
  NOINHERIT -- role does not automatically inherit privileges from parent roles
  -- NOLOGIN
  -- NOREPLICATION
  -- NOBYPASSRLS
  -- CONNECTION LIMIT -1
  -- PASSWORD NULL
  ;
CREATE ROLE

postgres=> \du+ rl_readonly
                            List of roles
  Role name  |          Attributes          | Member of | Description 
-------------+------------------------------+-----------+-------------
 rl_readonly | No inheritance, Cannot login | {}        | 
```

2. Une fois le rôle créé, nous lui donnons le privilège de se connecter à certaine DB :
```sql
GRANT CONNECT ON DATABASE <db_target> TO rl_readonly;
```

Exemple : 
```sql
postgres=> \conninfo 
You are connected to database "postgres" as user "postgres" on host "...eu-west-1.rds.amazonaws.com" (address "x.x.x.x") at port "5432".
SSL connection (protocol: TLSv1.2, cipher: AES128-SHA256, bits: 128, compression: off)

postgres=> \du postgres
                        List of roles
 Role name |          Attributes           |    Member of    
-----------+-------------------------------+-----------------
 postgres  | Create role, Create DB       +| {rds_superuser}
           | Password valid until infinity | 

postgres=> GRANT CONNECT ON DATABASE database_dev TO rl_readonly;
GRANT CONNECT ON DATABASE database_int TO rl_readonly;
GRANT
GRANT
```

3. La dernière étape consiste à autoriser uniquement le SELECT sur le schéma public de toutes les tables (et future tables) :
```sql
GRANT SELECT ON ALL TABLES IN SCHEMA public TO rl_readonly;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO rl_readonly;
```

Exemple : 
```sql
postgres=> \l+ database_dev
                                                            List of databases
     Name     |    Owner     | Encoding |   Collate   |    Ctype    |       Access privileges       |  Size   | Tablespace | Description 
--------------+--------------+----------+-------------+-------------+-------------------------------+---------+------------+-------------
 database_dev | database_dev | UTF8     | en_US.UTF-8 | en_US.UTF-8 | database_dev=CTc/database_dev+| 8411 kB | pg_default | 
              |              |          |             |             | rl_readonly=c/database_dev    |         |            | 
(1 row)

postgres=> \c database_dev database_dev
Password for user database_dev: 
psql (12.15, server 12.12)
SSL connection (protocol: TLSv1.2, cipher: AES128-SHA256, bits: 128, compression: off)
You are now connected to database "database_dev" as user "database_dev".

database_dev=> GRANT SELECT ON ALL TABLES IN SCHEMA public TO rl_readonly;
GRANT

database_dev=> ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO rl_readonly;
ALTER DEFAULT PRIVILEGES

-- List default privileges
database_dev=> \ddp
                 Default access privileges
    Owner     | Schema | Type  |     Access privileges      
--------------+--------+-------+----------------------------
 database_dev | public | table | rl_readonly=r/database_dev
(1 row)

-- Check Privilege on Table
database_dev=> SELECT
  r.rolname AS role_name,
  c.relname AS table_name,
  has_table_privilege(r.rolname, c.relname, 'SELECT') AS select_privilege,
  has_table_privilege(r.rolname, c.relname, 'INSERT') AS insert_privilege,
  has_table_privilege(r.rolname, c.relname, 'UPDATE') AS update_privilege,
  has_table_privilege(r.rolname, c.relname, 'DELETE') AS delete_privilege
FROM
  pg_roles r
  CROSS JOIN pg_class c
WHERE
  r.rolname <> 'rds_superuser' AND r.rolname NOT LIKE 'pg_%'
  AND c.relkind = 'r' -- Filtrer les relations de type table
  AND c.relname NOT LIKE 'sql\_%' ESCAPE '\'
  AND c.relname NOT LIKE 'pg\_%' ESCAPE '\'
ORDER BY
  r.rolname, c.relname;
    role_name    | table_name  | select_privilege | insert_privilege | update_privilege | delete_privilege 
-----------------+-------------+------------------+------------------+------------------+------------------
 database_dev    | departments | t                | t                | t                | t
 database_dev    | employees   | t                | t                | t                | t
 database_dev    | projects    | t                | t                | t                | t
 database_int    | departments | f                | f                | f                | f
 database_int    | employees   | f                | f                | f                | f
 database_int    | projects    | f                | f                | f                | f
 postgres        | departments | t                | t                | t                | t
 postgres        | employees   | t                | t                | t                | t
 postgres        | projects    | t                | t                | t                | t
 rds_ad          | departments | f                | f                | f                | f
 rds_ad          | employees   | f                | f                | f                | f
 rds_ad          | projects    | f                | f                | f                | f
 rds_iam         | departments | f                | f                | f                | f
 rds_iam         | employees   | f                | f                | f                | f
 rds_iam         | projects    | f                | f                | f                | f
 rds_password    | departments | f                | f                | f                | f
 rds_password    | employees   | f                | f                | f                | f
 rds_password    | projects    | f                | f                | f                | f
 rds_replication | departments | f                | f                | f                | f
 rds_replication | employees   | f                | f                | f                | f
 rds_replication | projects    | f                | f                | f                | f
 rdsadmin        | departments | t                | t                | t                | t
 rdsadmin        | employees   | t                | t                | t                | t
 rdsadmin        | projects    | t                | t                | t                | t
 rl_readonly     | departments | t                | f                | f                | f
 rl_readonly     | employees   | t                | f                | f                | f
 rl_readonly     | projects    | t                | f                | f                | f
(27 rows)
```
Répeter cette étape pour toute les DB cibles.

###  3.4. <a name='CrationdunutilisateurmembredeReadOnly'></a>Création d'un utilisateur membre de "ReadOnly"

Maintenant que notre rôle dispose des bons droits, nous allons pouvoir créer un utilisateur "INHERIT" et lui donner le droit d'hériter des privilèges du rôle "rl_readonly" (member of) :
```sql
CREATE ROLE "<usr_name>" WITH
  -- NOSUPERUSER
  -- NOCREATEDB
  -- NOCREATEROLE
  -- INHERIT
  LOGIN
  -- NOREPLICATION
  -- NOBYPASSRLS
  -- CONNECTION LIMIT -1
  ENCRYPTED PASSWORD 'azerty';

GRANT rl_readonly TO <usr_name>;
```

Exemple : 
```sql
postgres=> \conninfo 
You are connected to database "postgres" as user "postgres" on host "...eu-west-1.rds.amazonaws.com" (address "x.x.x.x") at port "5432".
SSL connection (protocol: TLSv1.2, cipher: AES128-SHA256, bits: 128, compression: off)

postgres=> \du postgres
                        List of roles
 Role name |          Attributes           |    Member of    
-----------+-------------------------------+-----------------
 postgres  | Create role, Create DB       +| {rds_superuser}
           | Password valid until infinity | 

postgres=> CREATE ROLE "usr_grafana" WITH
  -- NOSUPERUSER
  -- NOCREATEDB
  -- NOCREATEROLE
  -- INHERIT
  LOGIN
  -- NOREPLICATION
  -- NOBYPASSRLS
  -- CONNECTION LIMIT -1
  ENCRYPTED PASSWORD 'azerty';
CREATE ROLE

postgres=> GRANT rl_readonly TO usr_grafana;
GRANT ROLE

postgres=> \du+ usr_grafana
                     List of roles
  Role name  | Attributes |   Member of   | Description 
-------------+------------+---------------+-------------
 usr_grafana |            | {rl_readonly} | 

postgres=> \l
                                         List of databases
     Name     |    Owner     | Encoding |   Collate   |    Ctype    |       Access privileges       
--------------+--------------+----------+-------------+-------------+-------------------------------
 database_dev | database_dev | UTF8     | en_US.UTF-8 | en_US.UTF-8 | database_dev=CTc/database_dev+
              |              |          |             |             | rl_readonly=c/database_dev
 database_int | database_int | UTF8     | en_US.UTF-8 | en_US.UTF-8 | database_int=CTc/database_int+
              |              |          |             |             | rl_readonly=c/database_int
 postgres     | postgres     | UTF8     | en_US.UTF-8 | en_US.UTF-8 | postgres=CTc/postgres
 rdsadmin     | rdsadmin     | UTF8     | en_US.UTF-8 | en_US.UTF-8 | rdsadmin=CTc/rdsadmin
 template0    | rdsadmin     | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/rdsadmin                  +
              |              |          |             |             | rdsadmin=CTc/rdsadmin
 template1    | postgres     | UTF8     | en_US.UTF-8 | en_US.UTF-8 | postgres=CTc/postgres
(6 rows)

postgres=> \c database_dev usr_grafana
Password for user usr_grafana: 
psql (12.15, server 12.12)
SSL connection (protocol: TLSv1.2, cipher: AES128-SHA256, bits: 128, compression: off)
You are now connected to database "database_dev" as user "usr_grafana".

database_dev=> CREATE TABLE table01(
role_id serial PRIMARY KEY,
role_name VARCHAR (255) UNIQUE NOT NULL
);
ERROR:  permission denied for schema public
LINE 1: CREATE TABLE table01(
                     ^

database_dev=> SELECT * FROM departments;
 id |  name   | location 
----+---------+----------
  1 | IT      | New York
  2 | HR      | London
  3 | Finance | Tokyo
(3 rows)

database_dev=> DROP TABLE departments;
ERROR:  must be owner of table departments

database_dev=> \c postgres usr_grafana
Password for user usr_grafana: 
FATAL:  permission denied for database "postgres"
DETAIL:  User does not have CONNECT privilege.
Previous connection kept
```
---

[^1] https://www.postgresql.org/docs/14/ddl-schemas.html#DDL-SCHEMAS-PRIV

---

Source:
- AWS Documentation :
  - [Managing PostgreSQL users and roles](https://aws.amazon.com/fr/blogs/database/managing-postgresql-users-and-roles/)
  - [Understanding the rds_superuser role](https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Appendix.PostgreSQL.CommonDBATasks.Roles.html#Appendix.PostgreSQL.CommonDBATasks.Roles.rds_superuser)
- Other :
  - [A Beginner’s Guide to Role Inheritance and Policies for Postgres](https://cazzer.medium.com/postgres-role-inheritance-and-policies-for-beginners-4ae13f973bb1)
  - [Postgresql: How to add a role that inherits from another?](https://dba.stackexchange.com/questions/60942/postgresql-how-to-add-a-role-that-inherits-from-another)
  - [PostgreSQL : Comment créer un utilisateur et une base de données dans PostgreSQL à l'aide de l'interface CLI psql](https://www.stackhero.io/fr/services/PostgreSQL/documentations/Pour-commencer/Comment-creer-un-utilisateur-et-une-base-de-donnees-dans-PostgreSQL-a-l-aide-de-l-interface-CLI-psql)