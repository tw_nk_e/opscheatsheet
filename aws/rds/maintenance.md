# Maintenance

Voici ce qui se passe généralement lors d'un changement de classe d'instance sur RDS :
 
1. Lancement d'une nouvelle instance : Lorsque vous modifiez la classe d'instance, RDS lance une nouvelle instance de base de données avec la nouvelle classe. Cette nouvelle instance est créée en parallèle avec l'instance existante.
2. Copie des données : Une fois la nouvelle instance créée, RDS commence à copier les données de l'instance existante vers la nouvelle instance. La durée de cette opération dépend de la quantité de données à copier. Pendant cette étape, les opérations de lecture/écriture peuvent être suspendues sur l'instance d'origine.
3. Réplication continue : Une fois la copie des données terminée, RDS configure une réplication continue des données entre l'instance d'origine et la nouvelle instance. Cela garantit que les données restent synchronisées jusqu'à ce que la transition soit terminée.
4. Basculer la connexion : Une fois que la réplication est à jour et que la nouvelle instance est prête, RDS bascule la connexion vers la nouvelle instance. Cela peut impliquer une interruption de service de quelques secondes à quelques minutes, en fonction de la configuration et des options choisies.
5. Suppression de l'instance d'origine : Une fois la connexion basculée avec succès vers la nouvelle instance, l'instance d'origine est arrêtée et supprimée.

---

Source:
- AWS Documentation :
  - [Modifying an Amazon RDS DB instance](https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Overview.DBInstance.Modifying.html)