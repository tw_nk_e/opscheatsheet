# Querying Amazon VPC flow logs

<!-- vscode-markdown-toc -->
* 1. [Log file format (text)](#Logfileformattext)
* 2. [Athena configuration](#Athenaconfiguration)
* 3. [Exemples](#Exemples)
	* 3.1. [ Extract the log between two dates](#Extractthelogbetweentwodates)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Logfileformattext'></a>Log file format (text)

Log record format:

<img alt="Log Record Format" src="src/log_record_format.png" width="400">

##  2. <a name='Athenaconfiguration'></a>Athena configuration

Creating and querying a table for Amazon VPC flow logs using partition projection:
1. Create DB.
```sql
CREATE DATABASE vpc_flow_logs;
```
2. Select the DB.
3. Create the Table (respect the order of your log record format)
```sql
CREATE EXTERNAL TABLE IF NOT EXISTS vpc_flow_logs_table (
  account_id string,
  action string,
  az_id string,
  bytes bigint,
  dstaddr string,
  dstport int,
  `end` bigint,
  flow_direction string,
  instance_id string,
  interface_id string,
  log_status string,
  packets bigint,
  pkt_dst_aws_service string,
  pkt_dstaddr string,
  pkt_src_aws_service string,
  pkt_srcaddr string,
  protocol bigint,
  aws_region string,
  srcaddr string,
  srcport int,
  start bigint,
  sublocation_id string,
  sublocation_type string,
  subnet_id string,
  tcp_flags int,
  traffic_path int,
  type string,
  version int,
  vpc_id string
)
PARTITIONED BY (region string, day string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ' '
LOCATION 's3://awsexamplebucket/awsexampleprefix/awsexamplelogs/1111222233334444/vpcflowlogs/'
TBLPROPERTIES
(
"skip.header.line.count"="1",
"projection.enabled" = "true",
"projection.region.type" = "enum",
"projection.region.values" = "us-east-1,us-west-2,ap-south-1,eu-west-1",
"projection.day.type" = "date",
"projection.day.range" = "2021/01/01,NOW",
"projection.day.format" = "yyyy/MM/dd",
"storage.location.template" = "s3://awsexamplebucket/awsexampleprefix/awsexamplelogs/1111222233334444/vpcflowlogs/${region}/${day}"
)
```

##  3. <a name='Exemples'></a>Exemples
###  3.1. <a name='Extractthelogbetweentwodates'></a> Extract the log between two dates

This SQL query retrieves data from the `vpc_flow_logs_table` table, filters out records with a `srcaddr` matching the regular expression `172.31.\d.\d+`.  
It renames and converts the `start` and `end` columns into date formats.  
Then, it filters the results to include only records with a `start` date within a specific range and sorts the results in ascending order based on the `start` column.

```sql
SELECT
  "start" AS start_unixtime,
  from_unixtime ("start") AS start_timestamp,
  srcaddr,
  pkt_srcaddr,
  srcport,
  pkt_src_aws_service,
  dstaddr,
  pkt_dstaddr,
  dstport,
  pkt_dst_aws_service,
  protocol,
  "action",
  "end" AS end_unixtime,
  from_unixtime ("end") AS end_timestamp
FROM
  vpc_flow_logs_table
WHERE
  NOT REGEXP_LIKE (srcaddr, '172\.31\.\d\.\d+')
  AND from_unixtime ("start") >= TIMESTAMP '2023-09-11 00:00:00'
  AND from_unixtime ("start") <= TIMESTAMP '2023-10-09 23:59:59'
ORDER BY
  "start" ASC;
```

---

Source:
- AWS Documentation :
  - [How do I analyze the Amazon VPC flow logs using Amazon Athena?](https://repost.aws/knowledge-center/athena-analyze-vpc-flow-logs)
  - [Querying Amazon VPC flow logs](https://docs.aws.amazon.com/athena/latest/ug/vpc-flow-logs.html)