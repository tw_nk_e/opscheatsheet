
# Delete FSx Windows AD Computer

From the case description, I understand that you would like to understand how FSx for Windows works in an AD environment.

Please find below my input regarding this inquiry:

When you spin up a new FSX file system, 4 computer objects get created as part of the deployment MULTI-AZ FSx, 3 objects for a Single-AZ 2 file system, and 1 object for a Single-AZ 1 file system:

1. The file system's DNS name will have 2 IPs associated in DNS, and one computer object in AD.

2. The Windows Remote PowerShell Endpoint will also have a computer object in AD and it's IP address (in DNS) is the Primary private IPv4 IP of the ENI in the preferred subnet.

3. There will also be a computer object for the Primary private IPv4 IP of the ENI in the standby subnet.

4. Finally, another computer object in AD for the Failover cluster virtual network name account. This is for the FSx service itself since it's a managed service.

Currently, there is no efficient way for customers to identify the objects corresponding to each specific file system.

FSx team are aware of this and are considering updating the description of the AD objects to include the file system id as a feature release in the future.

What I can suggest however is this PowerShell command:
```powershell
Get-ADComputer -Filter 'Name -like "AMZN*"' -Properties IPv4Address,Created,Modified,Description | sort Created | FT Name,Description,DNSHostName,IPv4Address,Created,Modified -A
```
This command will display a list of AD objects used by FSX systems sorted with the creation date.

Now all you have to do is to check the creation date of your specific FSX to determine which AD objects are being used.