# Amazon S3 cheatsheet

<!-- vscode-markdown-toc -->
* 1. [Bucket Settings](#BucketSettings)
* 2. [Lifecycle rule actions](#Lifecycleruleactions)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='BucketSettings'></a>Bucket Settings

Block public access (bucket settings) = Off
```sh
aws s3api get-public-access-block \
    --bucket test-access-s3
{
    "PublicAccessBlockConfiguration": {
        "BlockPublicAcls": false,
        "IgnorePublicAcls": false,
        "BlockPublicPolicy": false,
        "RestrictPublicBuckets": false
    }
}
```

Block public access (bucket settings) = On
```sh
aws s3api get-public-access-block \
    --bucket test-access-s3
{
    "PublicAccessBlockConfiguration": {
        "BlockPublicAcls": true,
        "IgnorePublicAcls": true,
        "BlockPublicPolicy": true,
        "RestrictPublicBuckets": true
    }
}
```

Object Ownership = ACLs disabled : 
```sh
aws s3api get-bucket-ownership-controls --bucket test-access-s3
{
    "OwnershipControls": {
        "Rules": [
            {
                "ObjectOwnership": "BucketOwnerEnforced"
            }
        ]
    }
}
```

Object Ownership = ACLs enabled - Bucket owner preferred : 
```sh
aws s3api get-bucket-ownership-controls --bucket test-access-s3
{
    "OwnershipControls": {
        "Rules": [
            {
                "ObjectOwnership": "BucketOwnerPreferred"
            }
        ]
    }
}
```

Object Ownership = ACLs enabled - Object writer : 
```sh
aws s3api get-bucket-ownership-controls --bucket test-access-s3
{
    "OwnershipControls": {
        "Rules": [
            {
                "ObjectOwnership": "ObjectWriter"
            }
        ]
    }
}
```

##  2. <a name='Lifecycleruleactions'></a>Lifecycle rule actions

- `Expire current versions of object` (Expiration des versions courantes des objets) :  Cette règle permet de supprimer automatiquement les objets S3 après un certain nombre de jours suivant leur création. Elle s'applique aux objets qui n'ont pas de version spécifique (versions courantes) et aide à réduire les coûts de stockage en supprimant les objets obsolètes ou inutiles.

- `Permanently delete noncurrent versions of objects` (Suppression permanente des versions non courantes des objets) : Cette règle s'applique aux objets ayant la versioning activé. Elle permet de supprimer définitivement les versions non courantes (anciennes versions) des objets après un certain nombre de jours. Cela peut être utile pour réduire les coûts de stockage liés aux versions obsolètes tout en conservant les versions courantes des objets.

- `Delete expired object delete markers or incomplete multipart uploads` (Suppression des marqueurs de suppression d'objets expirés ou des téléversements multipartites incomplets) :

  - Delete markers (Marqueurs de suppression) : Lorsqu'un objet est supprimé dans un bucket versionné, un marqueur de suppression est créé. Cette règle permet de supprimer automatiquement ces marqueurs après un certain nombre de jours.

  - Incomplete multipart uploads (Téléversements multipartites incomplets) : Lorsqu'un téléversement d'objet est initié en plusieurs parties mais n'est pas terminé, il est considéré comme incomplet. Cette règle permet de supprimer automatiquement ces téléversements incomplets après un certain nombre de jours, ce qui peut aider à réduire les coûts de stockage.
