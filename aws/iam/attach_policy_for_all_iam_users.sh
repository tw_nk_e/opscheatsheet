#!/usr/bin/env bash
set -e
today_date=$(date +%Y%m%d)
policy_arn='arn:aws:iam::000000000000:policy/POL_MFA-MANDATORY'

log_msg() {
  echo "$(date '+%F %T') $@" >> ${today_date}_iam_user_policy_debug.log
}
cmd_log() {
  $@ 2>> ${today_date}_iam_user_policy_debug.log
}

# Extract all IAM User
aws iam list-users --query Users[].UserName --output json > ${today_date}_iam_username.json

# Read the JSON file and convert the array to a Bash array
readarray -t user_list <<< "$(jq -r '.[]' ${today_date}_iam_username.json)"
array_size=${#user_list[@]}
counter=1

for user in "${user_list[@]}"; do
  echo "Attach MFA policy to ${user} (${counter}/${array_size})"
  log_msg "Attach MFA policy to ${user}"
  cmd_log "aws iam attach-user-policy --policy-arn ${policy_arn} --user-name ${user}"
  log_msg "---"
  counter=$((counter + 1))
done