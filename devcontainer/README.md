# Availables tools

As of the latest revision of the image, the pre-installed tools are :
- [AWS CLI v2](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html)
- [AWS Vault](https://github.com/99designs/aws-vault)
- [Git](https://git-scm.com/)
- [Starship Cross-Shell Prompt](https://starship.rs/)
- [TFSwitch](https://tfswitch.warrensbox.com)