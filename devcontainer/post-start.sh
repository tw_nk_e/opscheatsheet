#!/usr/bin/env bash
set -e

# INIT COLOR
BOLD_CYAN=$'\e[1;36m'
COLOR_RESET=$'\e[0m'

# FLAG
notice_flag() {
  local NOTICE_FLAG="${BOLD_CYAN}[✦]${COLOR_RESET}"
  local text="$1"
  echo -e "${NOTICE_FLAG} ${text}"
}

case $(uname -m) in
  aarch64) arch_cpu="arm64" ;;
  x86_64) arch_cpu="amd64" ;;
esac

# AWS CLI - https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html#getting-started-install-instructions
notice_flag "AWS CLI Install."
yum install glibc groff less -y > /dev/null
curl -s "https://awscli.amazonaws.com/awscli-exe-linux-$(uname -m).zip" -o '/tmp/awscliv2.zip'
unzip /tmp/awscliv2.zip -d /tmp > /dev/null
/tmp/aws/install
rm -rf tmp/aws*
chmod -R 755 /usr/local/aws-cli/
# Command completion AWS
complete -C '/usr/local/bin/aws_completer' aws

# AWS Vault - https://github.com/99designs/aws-vault
notice_flag "AWS Vault Install."
git_version=$(git -c 'versionsort.suffix=-' \
  ls-remote --exit-code --refs --sort='version:refname' --tags https://github.com/99designs/aws-vault '*.*.*' \
  | tail --lines=1 \
  | cut --delimiter='/' --fields=3)
yum install pass gnupg wget pinentry -y > /dev/null
wget -q -O aws-vault "https://github.com/99designs/aws-vault/releases/download/${git_version}/aws-vault-linux-${arch_cpu}"
mv aws-vault /usr/local/bin/
chmod +x /usr/local/bin/aws-vault

# AWS Vault Configuration
notice_flag "AWS Vault Configuration."
echo "" >> ~/.bashrc
echo "# AWS Vault Configuration" >> ~/.bashrc
echo "export AWS_VAULT_BACKEND="pass"" >> ~/.bashrc
# Generate a key with gpg (gnupg)
# https://www.gnupg.org/documentation/manuals/gnupg-devel/Unattended-GPG-key-generation.html
notice_flag "GPG Configuration."
gpg --batch --gen-key <<EOF
%no-protection
Key-Type: RSA
Key-Length: 4096
Key-Usage: sign
Subkey-Type: RSA
Subkey-Length: 4096
Subkey-Usage: encrypt
Name-Real: aws-vault
Expire-Date: 0
EOF
# Create a storage key in pass from the previously generated public (pub) key
PUB_KEY=$(gpg --list-keys aws-vault | grep -E -o '[0-9A-F]{8}' | tr -d '\n')
notice_flag "Pass Configuration."
pass init "${PUB_KEY}"
# Alias
echo "" >> ~/.bashrc
echo "# AWS Vault Alias" >> ~/.bashrc
echo "alias av=aws-vault" >> ~/.bashrc
echo "alias avls='aws-vault ls'" >> ~/.bashrc
echo "alias avsh='aws-vault exec \$1'" >> ~/.bashrc

# Starship Configuration
notice_flag "Starship Configuration."
echo "" >> ~/.bashrc
echo "# Set Starship BASH as a prompt" >> ~/.bashrc
echo 'eval "$(starship init bash)"' >> ~/.bashrc
mkdir -p ~/.config && touch ~/.config/starship.toml
cp -f /tmp/starship.toml ~/.config/starship.toml

# Command completion Git
notice_flag "Git Configuration."
echo "" >> ~/.bashrc
echo "# Command completion Git" >> ~/.bashrc
echo "source /usr/share/bash-completion/completions/git" >> ~/.bashrc
# Alias
echo "" >> ~/.bashrc
echo "# Git Alias" >> ~/.bashrc
echo "alias gtl='git log --graph --all -n \$1'" >> ~/.bashrc
echo "alias gtll='git log --oneline --graph --all -n \$1'" >> ~/.bashrc
echo "alias gts='git status'" >> ~/.bashrc
echo "alias gtss='git status -sb'" >> ~/.bashrc
echo "alias gtpo='git push origin \$1'" >> ~/.bashrc